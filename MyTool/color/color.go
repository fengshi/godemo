package color

import (
"fmt"
"runtime"
)

const (
	TextBlack = iota + 30
	TextRed
	TextGreen
	TextYellow
	TextBlue
	TextMagenta
	TextCyan
	TextWhite
)
//https://github.com/fatih/color

/*
在其中 \x1b[ 实现CSI：
转换前景色为黑色，使用\x1b[30m
转换为红色，使用\x1b[31m
如果使用加粗参数，灰色写作\x1b[30;1m
获取红色加粗，使用\x1b[31;1m
重置颜色为缺省值，使用\x1b[39;49m （或者使用 \x1b[0m 重置所有属性）
\033[0m 重置为正常
\033[1m 设置高亮度或加粗
\033[4m 下划线
\033[5m 闪烁
\033[7m 反显
\033[8m 消隐
\033[30m -- /33[37m 设置前景色
\033[40m -- /33[47m 设置背景色
控制符ESC的常用表示方法\e、\x1b(\x1B)、\033都可以
\e 指代Escape，对应八进制\033，对应十六进制\x1b
*/
const (
	BackBlack = iota + 40
	BackRed
	BackGreen
	BackYellow
	BackBlue
	BackMagenta
	BackCyan
	BackWhite
)

func Black(str string) string {
	return textColor(TextBlack, str)
}

func Red(str string) string {
	return textColor(TextRed, str)
}

func Green(str string) string {
	return textColor(TextGreen, str)
}

func Yellow(str string) string {
	return textColor(TextYellow, str)
}

func Blue(str string) string {
	return textColor(TextBlue, str)
}

func Magenta(str string) string {
	return textColor(TextMagenta, str)
}

func Cyan(str string) string {
	return textColor(TextCyan, str)
}

func White(str string) string {
	return textColor(TextWhite, str)
}

func textColor(color int, str string) string {
	if IsWindows() {
		return str
	}

	switch color {
	case TextBlack:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextBlack, str)
	case TextRed:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextRed, str)
	case TextGreen:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextGreen, str)
	case TextYellow:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextYellow, str)
	case TextBlue:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextBlue, str)
	case TextMagenta:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextMagenta, str)
	case TextCyan:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextCyan, str)
	case TextWhite:
		return fmt.Sprintf("\x1b[0;%dm%s\x1b[0m", TextWhite, str)
	default:
		return str
	}
}

func backGroundClor(color int, str string)string  {
	st := fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 47: 白色 30: 黑 \n", 47,30)
	fmt.Println(st)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 47: 白色 30: 黑 \n", 47,30)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 46: 深绿 31: 红 \n", 46,31)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 45: 紫   32: 绿 \n", 45,32)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 44: 蓝   33: 黄 \n", 44,33)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 43: 黄   34: 蓝 \n", 43,34)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 42: 绿   35: 紫 \n", 42,35)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 41: 红   36: 深绿 \n", 41,36)
	fmt.Sprintf("\x1b[%d;%dmhello world \x1b[0m 40: 黑   37: 白色 \n", 40,37)
	return ""
}

func IsWindows() bool {
	if runtime.GOOS == "windows" {
		return true
	} else {
		return false
	}
}
