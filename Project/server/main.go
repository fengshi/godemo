package main

import "github.com/gin-gonic/gin"

func main()  {
	r := gin.Default()
	r.GET("/live", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"message": "success",
		})
	})

	err := r.Run(":9999")
	if err != nil {
		panic(err)
	}
}
