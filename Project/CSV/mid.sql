CREATE TABLE IF NOT EXISTS `movie_ID` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` varchar(10) DEFAULT '' COMMENT 'movie Id',
  `year` varchar(30) DEFAULT '' COMMENT '年份',
  `star` float(10) unsigned DEFAULT '0' COMMENT 'star',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='豆瓣电影ID';
