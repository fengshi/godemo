package main

import (
	"database/sql"
	"encoding/csv"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main()  {

	db, err := sql.Open("mysql", "root:qjn19920314@tcp(localhost:3306)/douban")
	if err != nil {
		return
	}
	defer db.Close()

	CreateTable(db)
   //  insert(db, "12313", "1992", 6.6)
	ReadCsv_ConfigFile_Fun(db)


}

func ReadCsv_ConfigFile_Fun(db *sql.DB) {
	// 获取数据，按照文件
	file := os.ExpandEnv("$GOPATH") + "/src/goDemo/Project/CSV/sample.csv"

	os.Open(file)
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		return
	}
	r := csv.NewReader(strings.NewReader(string(dat[:])))

	t:= 0
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			continue
		}
		fmt.Println("-----------------------------")
		if t >= 1 {
			ID := record[0]
			year := record[7]

			if strings.Contains(year,"(") {
				year = strings.Split(year, "(")[0]
			}
			if len(year) > 20 {
				year = year[:20]
			}

			starStr := record[11]

			var star float64 = 0
			if s, err := strconv.ParseFloat(starStr, 64); err == nil {
				star = s
			}

			//fmt.Println(ID, year, star)
			insert(db, ID, year, float32(star))

		}
		t += 1
	}


}
func CreateTable(db *sql.DB) {

	file := os.ExpandEnv("$GOPATH") + "/src/goDemo/Project/CSV/mid.sql"
	buf, err := ioutil.ReadFile(file)
	res, err := db.Exec(string(buf))

	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()

	fmt.Println("create count", count)
}


func insert(db *sql.DB, ID, y string, s float32) {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println(ID, y, s)
		}

	}()

	res, err := db.Exec("insert into movie_ID(mid, year, star)values(?, ?, ?)", ID, y, s)
	if err != nil {
		fmt.Println(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}

	fmt.Println("insert new record", id)

}