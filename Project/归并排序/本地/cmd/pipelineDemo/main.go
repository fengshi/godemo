package main

import (
	"bufio"
	"fmt"
	"goDemo/Project/goroutin/pipeline"
	"math/rand"
	"os"
)

//ls -lrt
func main()  {

	const flieName  = "test.in"
	const size  = 64
	file,err := os.Create(flieName)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	p := randomSource(size)
	writer := bufio.NewWriter(file)
	pipeline.WriteSink(writer, p)
	writer.Flush()


	file, err = os.Open(flieName)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	p = pipeline.ReadSource(bufio.NewReader(file),-1)

	for v := range p {
		fmt.Println(v)
	}
}

func mergeDemo()  {
	s1 := pipeline.InMemSort(pipeline.ArraySource(2,4,5,6,1,8,3,2))
	s2 := pipeline.InMemSort(pipeline.ArraySource(3,1,7,2,9,0))
	p := pipeline.Merge(s1, s2)
	for v := range p { //通道需要关闭, 否则 range 无法知道何时结束
		fmt.Println(v)
	}
}

func randomSource(count int) <-chan int  {
	out := make(chan int)

	go func() {
		for i := 0; i < count; i++ {
			out <- rand.Int()
		}
		close(out)
	}()

	return out
}