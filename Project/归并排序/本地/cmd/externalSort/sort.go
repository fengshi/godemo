package main

import (
	"bufio"
	"fmt"
	"goDemo/Project/goroutin/pipeline"
	"os"
)

func main()  {

	p := createPipeline("test.in", 800000000, 4)

	writeToFile(p,"test.out")

	printFile("test.out")
}

func printFile(filename string)  {
	file,err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	p := pipeline.ReadSource(file, -1)

	count := 0
	for v := range p {
		fmt.Println(v)
		if count >= 100 {
			break
		}
		count++
	}
}
func writeToFile(p <-chan int,filename string)  {

	file,err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	defer writer.Flush()

	pipeline.WriteSink(writer, p)
	
}
func createPipeline(filename string, filesize, chunkCount int) <- chan int  {
	chunkSize := filesize / chunkCount

	pipeline.Init()
	sortResults := []<-chan int{}
	for i := 0;i < chunkCount;i++ {
		file, err := os.Open(filename)
		if err != nil {
			panic(err)
		}
		file.Seek(int64(i * chunkSize), 0)
		source := pipeline.ReadSource(bufio.NewReader(file), chunkSize)
		sortResults = append(sortResults,pipeline.InMemSort(source))
	}
	return pipeline.MergeN(sortResults...)
}