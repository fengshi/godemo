package pipeline

import (
	"encoding/binary"
	"fmt"
	"io"
	"sort"
	"time"
)

var startTime time.Time

func Init()  {
	startTime = time.Now()
}


func ArraySource(a ...int) <-chan int  {
	out := make(chan int, 1024)
	go func() {
		for _,v := range a {
			out <- v
		}
		close(out)
	}()
	return out
}

func InMemSort(in <-chan int) <-chan int  {
	out := make(chan int, 1024)
	go func() {
		//read into memory
		a := []int{}
		for v := range in {
			a = append(a, v)
		}
		fmt.Println("read Done:", time.Now().Sub(startTime))

		//sort
		sort.Ints(a)
		fmt.Println("in memory sort Done:", time.Now().Sub(startTime))
		//output
		for _, v := range a {
			out <- v
		}

		close(out)

	}()

	return out
}

func Merge(in1, in2 <- chan int) <- chan int  {
	out := make(chan int, 1024)

	go func() {
		v1, ok1 := <- in1
		v2, ok2 := <- in2

		for ok1 || ok2 {
			if !ok2 || (ok1 && v1 <= v2){
				out <- v1
				v1, ok1 = <- in1
			} else {
				out <- v2
				v2, ok2 = <- in2
			}
		}
		fmt.Println("merge Done:", time.Now().Sub(startTime))
		close(out)

	}()
	return out
}

func ReadSource(reader io.Reader, chunkSize int) <- chan int {
	out := make(chan int, 1024)

	go func() {
		buffer := make([]byte, 8)
		bytesRead := 0
		for {
			n,err := reader.Read(buffer)
			bytesRead += n
			if n > 0 {
				v := int(binary.BigEndian.Uint64(buffer))
				out <- v
			}
			if err != nil || (chunkSize != -1 && bytesRead >= chunkSize) {
				break
			}
		}
		close(out)
	}()
	return out
}

func WriteSink(writer io.Writer, in <-chan int)  {
	for v := range in {
		buffer := make([]byte, 8)
		binary.BigEndian.PutUint64(buffer, uint64(v))
		writer.Write(buffer)
	}
}

func MergeN(ins ...<-chan int)<- chan int  {
	if len(ins) == 1 {
		return ins[0]
	}

	m := len(ins) / 2

	return Merge(MergeN(ins[:m]...), MergeN(ins[m:]...))


}


