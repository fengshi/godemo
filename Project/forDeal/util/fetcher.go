package util

import (
	"bufio"
	"encoding/json"
	"fmt"
	"goDemo/Project/forDeal/model"
	"goDemo/Project/forDeal/tool"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"mcw/STO/libs/common"
)

// 单个ID 最多 100 页


var pageLimit = 50

var bufferSize = 1000
//http://m.fordeal.com/itemlist/70001061/1
var ItemList = "http://m.fordeal.com/itemlist/%v/%v"


func fetcher(catID int, title, name, filep string)  {


	for i := 1;i <= pageLimit;i ++ {

		url := fmt.Sprintf(ItemList, catID, i)

		buf, err := tool.HttpGet(url)
		if err != nil {
			continue
		}

		if len(buf) == 0 {
			break
		}

		var items  model.ItemList
		err = json.Unmarshal(buf, &items)
		if err != nil {
			continue
		}
		fmt.Println(url, items)
		for _, item := range  items {
			//图片名称：价格+ 产品ID
			imageName := fmt.Sprintf("%v-%v.%v", item.DisplayDiscountPrice, item.ID,"jpg")

			_ = Download(item.DisplayImage, filepath.Join(filep, imageName))

		}


	}



}


func Download(url string, filePath string) error {

	if common.FileExists(filePath) {
		return fmt.Errorf(fmt.Sprintln("文件已存在", url))
	}
	res,  err := http.Get(url)
	if err != nil{
		return err
	}
	defer res.Body.Close()

	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	defer file.Close()
	if err != nil {
		fmt.Println("open log file err", err)
		return err
	}
	w := bufio.NewWriterSize(file, bufferSize)
	defer w.Flush()

	result, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	_ , err = w.Write(result)
	return err
}