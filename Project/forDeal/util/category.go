package util

import (
	"encoding/json"
	"fmt"
	"goDemo/Project/forDeal/model"
	"io/ioutil"
	"mcw/STO/libs/common"
	"os"
	"path/filepath"
)

var categoryUrl = "http://m.fordeal.com/api/category"

var rootPath = os.ExpandEnv("$HOME") + "/Downloads" + "/forDeal"

func GetCategory() model.Category {

	//buf, err := util.HttpGet(categoryUrl)
	//if err != nil {
	//	panic(err)
	//}

	file, err := os.Open("category.json")

	if err != nil {

		path := os.ExpandEnv("$HOME") + "/Desktop/forDeal/category.json"

		file, err = os.Open(path)
		if err != nil {
			panic(" 配置文件为空")
		}
	}

	buf, _ := ioutil.ReadAll(file)

	var category model.Category

	_ = json.Unmarshal(buf, &category)

	return category

}

func Run() {

	category := GetCategory()

	if common.FileExists(rootPath) == false {
		_ = os.MkdirAll(rootPath, 0777)
	}

	fmt.Println("数据拉取中，一共有大品类",len(category.Data) ,"种")
	for _, v := range category.Data {

		file := filepath.Join(rootPath, v.Title)
		if common.FileExists(rootPath) == false {
			_ = os.MkdirAll(file, 0777)
		}

		fetcher(v.CatID, v.Title, v.Name, file)

		for _, v1 := range v.Sons {

			file := filepath.Join(file, v1.Title)
			if common.FileExists(file) == false {
				_ = os.MkdirAll(file, 0777)
			}

			fetcher(v1.CatID, v1.Title, v1.Name, file)

			for _, v2 := range v1.Sons {

				file := filepath.Join(file, v2.Title)
				if common.FileExists(file) == false {
					_ = os.MkdirAll(file, 0777)
				}

				fetcher(v2.CatID, v2.Title, v2.Name, file)
			}
		}

	}

	fmt.Println(rootPath)

}
