package model


type Category struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data []struct {
		ID         int    `json:"id"`
		CatID      int    `json:"cat_id"`
		Name       string `json:"name"`
		Img        string `json:"img"`
		CanViewAll bool   `json:"can_view_all"`
		Sons       []struct {
			ID    int    `json:"id"`
			CatID int    `json:"cat_id"`
			Name  string `json:"name"`
			Img   string `json:"img"`
			Sons  []struct {
				ID         int    `json:"id"`
				CatID      int    `json:"cat_id"`
				Name       string `json:"name"`
				Img        string `json:"img"`
				CanViewAll bool   `json:"can_view_all"`
				Title      string `json:"title"`
			} `json:"sons"`
			CanViewAll bool   `json:"can_view_all"`
			Title      string `json:"title"`
		} `json:"sons"`
		Title string `json:"title"`
	} `json:"data"`
}


type ItemList []struct {
	ID                   int      `json:"id"`
	Status               int      `json:"status"`
	DisplayImage         string   `json:"display_image"`
	Discount             float64  `json:"discount"`      // 卖掉的 百分比
	IsAdmin              bool     `json:"is_admin"`
	LeftType             int      `json:"left_type"`
	DiscountImg          string   `json:"discount_img"`
	Ctm                  string   `json:"ctm"`
	Cur                  string   `json:"cur"`
	URL                  string   `json:"url"`
	NativeURL            []string `json:"native_url"`
	Title                string   `json:"title"`
	LikeNum              int      `json:"like_num"`            //like 数
	IsDiscount           bool     `json:"is_discount"`
	DiscountTimerSeconds int      `json:"discount_timer_seconds"`
	DisplayDiscountPrice string   `json:"display_discount_price"` //优惠价
	DisplayOriginalPrice string   `json:"display_original_price"`  //原价
	DisplayTags          []struct {
		ID    int    `json:"id"`
		Desc  string `json:"desc"`
		KeyID int    `json:"key_id"`
	} `json:"display_tags"`
}

type DownItem struct {
	Url string
	Path string
	LikeNum int
	Price string
}



type DownItems []*DownItem
type ByDownItemLikeNum struct{ DownItems }
func (r DownItems) Len() int      { return len(r) }
func (r DownItems) Swap(i, j int) { r[i], r[j] = r[j], r[i] }
func (r DownItems) Less(i, j int) bool { return r[i].LikeNum > r[j].LikeNum }
