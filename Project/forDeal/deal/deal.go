package deal

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"mcw/blockChain/common/util"
	"myTool/file"
	"os"
	"mcw/STO/libs/common"
	"strconv"
)
var rootPath = os.ExpandEnv("$HOME") + "/Downloads" + "/forDeal"
var Config dealCon

type dealCon struct {
	CatId int64 `json:"catId"`
	Limit []int64	`json:"limit"`
	DetailIds []int64`json:"detailIds"`
}

func init() {
	LoadConfig()
}

func Run()  {
	fmt.Println(Config)

	if Config.CatId > 0 {
		path := rootPath + "/" + strconv.Itoa(int(Config.CatId))
		if common.FileExists(path) == false {
			err := os.Mkdir(path,0777)
			if err != nil {
				panic(err)
			}

		}
		if Config.CatId > 0 {
			fetch(Config.CatId, path)
		}
	}


	if len(Config.DetailIds) > 0 {
		fetchDetails(Config.DetailIds)
	}

}

func LoadConfig()dealCon  {

	path := ConfigPath()

	if file.PathExist(path) == false {

		panic("配置文件找不到")
	}
	if _, err := toml.DecodeFile(path, &Config); err != nil {
		fmt.Println("配置填写有误")
	}
	fmt.Println("配置文件信息：", Config)
	return Config

}

func ConfigPath() string {
	conPath := mcw_util.GetCurrentDirectory() + "/config.toml"
	if mcw_util.FileExists(conPath) == true {
		return conPath
	}

	gopath := os.Getenv("$HOME")

	absolutePath := gopath + "/Desktop/forDeal/config.toml"
	if mcw_util.FileExists(absolutePath) {
		return absolutePath
	}
	pp := mcw_util.GetCurrentDirectory()+"/Desktop/forDeal/config.toml"
	if mcw_util.FileExists(pp) {
		return pp
	}


	return "/Users/lw/Desktop/forDeal/config.toml"

}