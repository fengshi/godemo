package tool

import (
	"io/ioutil"
	"ketang/netWork/help"
	"net/http"
	"time"
)

func HttpGet(url string) ([]byte, error) {
	client := http.Client{}
	client.Timeout = time.Second * 50

	if resp, err := client.Get(url); err != nil {
		return nil, err
	} else {
		defer resp.Body.Close()

		result, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return result,nil
	}
}

func HttpGetRequest(url string)([]byte, error)  {
	client := http.Client{}
	//2.创建一个请求
	request, err := http.NewRequest("GET", url, nil)
	help.CheckErr(err)
	//3.客户端发送请求
	region := &http.Cookie{Name:"region", Value:"US"}
	uid := &http.Cookie{Name:"uuid", Value:"web_5ca56f2264afb991430634"}
	cur := &http.Cookie{Name:"cur", Value:"SAR"}
	lan := &http.Cookie{Name:"lan", Value:"en"}
	download := &http.Cookie{Name:"download", Value:"1"}
	token := &http.Cookie{Name:"XSRF-TOKEN", Value:"eyJpdiI6InFxNVViNVFJNnVkMmJOdnhoczVVVmc9PSIsInZhbHVlIjoiYjBxdytLaE5oSWV0MjFZczY5NTJNalBHMThEc05GZ1ZJUG1GSTZRNDRxWEZmcGRLOVwvN3BWbW9YZUVYSzBkaHhDR3Z2TEY1MFFLakkzY3c0WmNFTElnPT0iLCJtYWMiOiJjMjJmODhhNGE3MTE3YTU4MjdkNTQ5NTVjODdmM2ZjZDhhNDZkNzNhYzAxZmRjNzZjZWE5MTI5MzM4NTg3YWM0In0"}


	//添加cookie
	request.AddCookie(region)
	request.AddCookie(uid)
	request.AddCookie(cur)
	request.AddCookie(lan)
	request.AddCookie(download)
	request.AddCookie(token)
	response, err := client.Do(request)

	//设置请求头
	request.Header.Set("Accept-Lanauage", "zh-cn")
	request.Header.Set("zh", "q=0")
	request.Header.Set("en", "q=0.8")

	request.Header.Set("User-Agent","User-Agent	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36")

	help.CheckErr(err)
	defer response.Body.Close()

	result, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return result,nil

}