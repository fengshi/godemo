package rpcDemo

import "errors"

type DemoService struct {}

type Args struct {
	A,B int
}

// result 必须是指针类型
func (DemoService)Div(args Args, result *float64) error  {
	if args.B == 0 {
		return errors.New("division by zero")
	}
	*result = float64(args.A) / float64(args.B)
	return nil
}

//使用终端测试
/*
1. 开启service服务
2. telnet localhost 12345
3. {"method":"DemoService.Div","params":[{"A":3,"B":4}],"id":1}

{"method":"DemoService.Div","params":[{"A":3,"B":0}],"id":2}


*/