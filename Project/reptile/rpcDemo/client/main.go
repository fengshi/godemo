package main

import (
	"fmt"
	"goDemo/Project/reptile/rpcDemo"
	"math/rand"
	"net"
	"net/rpc/jsonrpc"
)

func main() {
	conn, err := net.Dial("tcp",":12345")
	if err != nil {
		panic(err)
	}
	client := jsonrpc.NewClient(conn)

	var re float64
	for i := 10 ; i>=0 ;i-- {
		r:= rand.Intn(10)
		err = client.Call("DemoService.Div",rpcDemo.Args{i,r}, &re)
		if err != nil {
			println(err.Error())
		} else {
			fmt.Println(i, r, re)
		}
	}
}
