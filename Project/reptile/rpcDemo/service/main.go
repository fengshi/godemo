package main

import (
	"fmt"
	"goDemo/Project/reptile/rpcDemo"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

func main() {
	rpc.Register(rpcDemo.DemoService{})

	listener, err := net.Listen("tcp",":12345")
	if err != nil {
		 panic(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("accept conn error")
			continue
		}

		go jsonrpc.ServeConn(conn)
	}

}
