package RedisTest

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"myTool/common"
	"time"
)

/******************************************************************************
set(key, value)：给数据库中名称为key的string赋予值value
get(key)：返回数据库中名称为key的string的value
getset(key, value)：给名称为key的string赋予新值的时候 可以获取到旧值，相当于先get 再set
mset(k1,v1,k2,v2...) 批量储存多个kv
mget(key1, key2,…, key N)：返回库中多个string的value

setnx(key, value)：仅当键值不存在时设置键值.
setex(key, time, value)：向库中添加string，设定过期时间time
msetnx(key N, value N)：如果所有名称为key i的string都不存在

incr(key)：名称为key的string增1操作
incrby(key, integer)：名称为key的string增加integer
decr(key)：名称为key的string减1操作
decrby(key, integer)：名称为key的string减少integer
append(key, value)：名称为key的string的值附加value
substr(key, start, end)：返回名称为key的string的value的子串
strlen(key):返回 key 所储存的字符串值的长度。

*******************************************************************************/

/// 特殊参数，这些参数可以用于上面所有命令
//EX seconds -- 指定过期时间，单位为秒.
//PX milliseconds -- 指定过期时间，单位为毫秒.
//NX -- 仅当键值不存在时设置键值.
//XX -- 仅当键值存在时设置键值.

var cc redis.Conn
func init() {
	var err error
	cc, err = Login()
	if err != nil {
		panic(err)
	}
}

func SetAndGet()  {
	defer cc.Close()
	_, setErr := cc.Do("SET", "set_key", "xx.github.io")
	common.CheckErr(setErr)
	//使用redis的string类型获取set的k/v信息
	r, getErr := redis.String(cc.Do("GET", "set_key"))
	common.CheckErr(getErr)
	fmt.Println(r)


	_, setErr = cc.Do("SET", "set_key2", "xx.github.io22","EX",2)
	common.CheckErr(setErr)
	time.Sleep(3 * time.Second)
	r, getErr = redis.String(cc.Do("GET", "set_key2"))
	common.CheckErr(getErr)
	fmt.Println(r)


}

func Getset()  {
	defer c.Close()
	mydb,err := redis.String(c.Do("GETSET","mydb","mysql2"))
	if err != nil{
		fmt.Println("getset failed",err.Error())
	}
	fmt.Println("get before",mydb)
	mydb,err = redis.String(c.Do("get","mydb"))
	if err == nil{
		fmt.Println("mydb:",mydb)
	}
}

func MsetGet1()  {
	defer c.Close()
	var keys []interface{}
	var values []interface{}
	for i := 0; i < 10; i++ {
		keys = append(keys, i)
		values = append(values, i, fmt.Sprintf("--%v==", i))
	}

	fmt.Println(values, len(values))
	err := c.Send("MSET", values...)
	common.CheckErr(err)


	r,getErr := redis.Strings(c.Do("MGET", keys...))
	common.CheckErr(getErr)

	fmt.Println(r)
}

func MsetGet2()  {
	defer c.Close()
	var keys []interface{}
	values := redis.Args{}
	for i := 10; i < 20; i++ {
		keys = append(keys, i)
		values = values.Add(i).Add(fmt.Sprintf("--%v==", i))
	}

	fmt.Println(values, len(values))
	err := c.Send("MSET", values...)
	common.CheckErr(err)


	r,getErr := redis.Strings(c.Do("MGET", keys...))
	common.CheckErr(getErr)

	fmt.Println(r)
}

func SetNx()  {
	defer c.Close()
	_,err := c.Do("setnx","foo","bluegogo")
	_,err = c.Do("setnx","foo","bluegogo1")
	if err != nil{
		fmt.Println("setnx failed",err.Error())
	}
	foo,err := redis.String(c.Do("get","foo"))
	if err == nil{
		fmt.Println("setnx value",foo)
	}

	//或者 这样, 效果一样
	_,err = c.Do("set","foo2","mobai","NX")
	_,err = c.Do("set","foo2","mobai2","NX")
	if err != nil{
		fmt.Println("setnx failed",err.Error())
	}
	foo,err = redis.String(c.Do("get","foo2"))
	if err == nil{
		fmt.Println("setnx value",foo)
	}
}

func MsetNx()  {
	defer c.Close()
	_,err := c.Do("msetnx","bike1","ofo","bike2","bluegogo","bike3","foo")
	if err != nil{
		fmt.Println("setnx failed")
	}
	var tmpbike1 string
	var tmpbike2 string
	var tmpbike3 string
	array,err :=redis.Values( c.Do("MGET","bike1","bike2","bike3"))
	if err != nil{
		fmt.Println("mget failed",err.Error())
	}else{
		if _,err = redis.Scan(array,&tmpbike1,&tmpbike2,&tmpbike3);err == nil{
			fmt.Println("mget :",tmpbike1,tmpbike2,tmpbike3)
		}
	}
}

//名称为key的string增1操作
func Incr()  {
	defer c.Close()
	_,err := c.Do("SET","myage",10)
	if err != nil{
		fmt.Println("set myage failed",err.Error())
	}
	myage,err := redis.String(c.Do("GET","myage"))
	if err == nil{
		fmt.Println("before myage",myage)
	}
	_,err=c.Do("INCR","myage")
	if err != nil{
		fmt.Println("incr error",err.Error())
	}

	myage,err = redis.String(c.Do("GET","myage"))
	if err == nil{
		fmt.Println("after myage",myage)
	}

}

//名称为key的string增 n 操作
func Incrby()  {
	defer c.Close()
	_,err := c.Do("SET","myage",10)
	if err != nil{
		fmt.Println("set myage failed",err.Error())
	}
	myage,err := redis.String(c.Do("GET","myage"))
	if err == nil{
		fmt.Println("before myage",myage)
	}
	_,err=c.Do("INCRBY","myage",15)
	if err != nil{
		fmt.Println("incr error",err.Error())
	}

	myage,err = redis.String(c.Do("GET","myage"))
	if err == nil{
		fmt.Println("after myage",myage)
	}

}



//名称为key的string减1操作
func Decr()  {
	defer c.Close()
	_,err := c.Do("SET","cad",20)
	if err != nil{
		fmt.Println("set cad failed",err.Error())
	}
	myage,err := redis.String(c.Do("GET","cad"))
	if err == nil{
		fmt.Println("before cad",myage)
	}
	_,err=c.Do("DECR","cad")
	if err != nil{
		fmt.Println("decr error",err.Error())
	}

	myage,err = redis.String(c.Do("GET","cad"))
	if err == nil{
		fmt.Println("after cad",myage)
	}

}

//名称为key的string减 n 操作
func Decrby()  {
	defer c.Close()
	_,err := c.Do("SET","cad",20)
	if err != nil{
		fmt.Println("set cad failed",err.Error())
	}
	myage,err := redis.String(c.Do("GET","cad"))
	if err == nil{
		fmt.Println("before cad",myage)
	}
	_,err=c.Do("DECRBY","cad",15)
	if err != nil{
		fmt.Println("decrby error",err.Error())
	}

	myage,err = redis.String(c.Do("GET","cad"))
	if err == nil{
		fmt.Println("after cad",myage)
	}

}

// 在旧的value 上追加 value
func Append() {
	defer c.Close()
	_, setErr := c.Do("APPEND", "name", "zhangsan")
	common.CheckErr(setErr)

	r, getErr := redis.String(c.Do("GET", "name"))
	common.CheckErr(getErr)

	fmt.Println(r)

}

func Substr()  {
	defer c.Close()
	_, _ = c.Do("SET", "substr-bike", "abcdefgh")
	sub,err := redis.String(c.Do("substr","substr-bike",1,4))
	if err != nil{
		fmt.Println("substr get err",err.Error())
	}
	fmt.Println("sub", sub)
}

func Strlen()  {
	defer c.Close()
	_, _ = c.Do("SET", "sublen-bike", "abcdefghafaa")
	sub,err := redis.Int64(c.Do("STRLEN","sublen-bike"))
	if err != nil{
		fmt.Println("sublen get err",err.Error())
	}
	fmt.Println("sub", sub)
}