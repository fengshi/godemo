package RedisTest

import (
	"fmt"
	"go-common/library/cache/redis"
	"myTool/common"
	"time"
)

/******************************************************************************
1	DEL key
该命令用于在 key 存在时删除 key。
2	DUMP key
序列化给定 key ，并返回被序列化的值。
3	EXISTS key
检查给定 key 是否存在。
4	EXPIRE key seconds
为给定 key 设置过期时间，以秒计。
5	EXPIREAT key timestamp
EXPIREAT 的作用和 EXPIRE 类似，都用于为 key 设置过期时间。 不同在于 EXPIREAT 命令接受的时间参数是 UNIX 时间戳(unix timestamp)。
6	PEXPIRE key milliseconds
设置 key 的过期时间以毫秒计。
7	PEXPIREAT key milliseconds-timestamp
设置 key 过期时间的时间戳(unix timestamp) 以毫秒计
8	KEYS pattern
查找所有符合给定模式( pattern)的 key 。
9	MOVE key db
将当前数据库的 key 移动到给定的数据库 db 当中。
10	PERSIST key
移除 key 的过期时间，key 将持久保持。
11	PTTL key
以毫秒为单位返回 key 的剩余的过期时间。
12	TTL key
以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)。
13	RANDOMKEY
从当前数据库中随机返回一个 key 。
14	RENAME key newkey
修改 key 的名称
15	RENAMENX key newkey
仅当 newkey 不存在时，将 key 改名为 newkey 。
16	TYPE key
返回 key 所储存的值的类型。
******************************************************************************/

func Del()  {
	defer c.Close()
	_, setErr := c.Do("SET", "a", "xx.github.io")
	common.CheckErr(setErr)

	_, err := c.Do("DEL","a")
	if err != nil {
		fmt.Println("del fail", err)
	}
	r, err := redis.String(c.Do("GET", "a"))
	fmt.Println("get a",r, err)
}

func Dump()  {
	
}

func Exist()  {
	_, _ = c.Do("SET", "a", "xx.github.io")
	isExist, err := c.Do("EXISTS", "a")
	if err != nil {
		fmt.Println("hexist failed", err.Error())
	} else {
		fmt.Println("exist or not:", isExist.(int64) == 1)
	}
}


func Expire()  {
	_, _ = c.Do("SET", "a", "xx.github.io")
	_, _ = c.Do("EXPIRE", "a", 1)

	time.Sleep(time.Second * 2)
	r, err := redis.String(c.Do("GET","a"))
	fmt.Println("get:", r, err)
}

//移除 key 的过期时间，key 将持久保持。
func PERSIST()  {
	_, _ = c.Do("SET", "b", "xx.github.ioafsdf","EX",1)
	_, _ = c.Do("PERSIST", "b")
	time.Sleep(time.Second * 2)
	r, err := redis.String(c.Do("GET","b"))
	fmt.Println("get:", r, err)
}

// 查看过期时间
// PTTL 以毫秒为单位返回 key 的剩余的过期时间。
// TTL  以秒为单位返回 key 的剩余的过期时间。
func PTTL()  {
	_, _ = c.Do("SET", "a", "redis.pttl","EX",10)
	time.Sleep(time.Second * 3)

	r, err := redis.Int64(c.Do("PTTL", "a"))
	if err != nil {
		fmt.Println("PTTL error", err)
	} else {
		fmt.Println("pttl time",r)
	}

}


func TTL()  {
	_, _ = c.Do("SET", "abc", "redis.pttl","EX",100)
	time.Sleep(time.Second * 3)

	r, err := redis.Int64(c.Do("TTL", "abc"))
	if err != nil {
		fmt.Println("TTL error", err)
	} else {
		fmt.Println("ttl time",r)
	}

}
//RANDOMKEY
//从当前数据库中随机返回一个 key
func PandomKey()  {
	_, _ = c.Do("SET", "b", "xx.github.io")
	_, _ = c.Do("SET", "c", "xx.github.io")
	_, _ = c.Do("SET", "d", "xx.github.io")
	r, err := redis.String(c.Do("RANDOMKEY"))
	fmt.Println(r, err)
}

//RENAME key newkey
//修改 key 的名称

//RENAMENX key newkey
//仅当 newkey 不存在时，将 key 改名为 newkey
func Rename()  {
	_, _ = c.Do("SET", "name", "alice")
	_, _ = c.Do("RENAME", "name", "bob")
	r, err := redis.String(c.Do("GET","name"))
	fmt.Println(r, err)
}

//TYPE key
//返回 key 所储存的值的类型。
// ？？？返回的类型都是string
func TypeKey() {
	_, _ = c.Do("SET", "c1", "alice")
	_, _ = c.Do("SET", "c2", []byte("bob"))
	_, _ = c.Do("SET", "c3", 0x11)
	r, err := redis.String(c.Do("TYPE", "c1"))
	fmt.Println(r, err)

	r2, err2 := c.Do("TYPE", "c2")
	fmt.Println(r2, err2)

	r3, err3 := c.Do("TYPE", "c3")
	fmt.Println(r3, err3)
}