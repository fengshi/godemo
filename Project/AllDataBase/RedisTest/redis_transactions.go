package RedisTest

import (
	"fmt"
	"go-common/library/cache/redis"
)

// 事务
//以 MULTI 开始一个事务， 然后将多个命令入队到事务中， 最后由 EXEC 命令触发事务
//单个 Redis 命令的执行是原子性的，但 Redis 没有在事务上增加任何维持原子性的机制，所以 Redis 事务的执行并不是原子性的。
//
//事务可以理解为一个打包的批量执行脚本，但批量指令并非原子化的操作，中间某条指令的失败不会导致前面已做指令的回滚，也不会造成后续的指令不做。


/*
redis 127.0.0.1:7000> multi
OK
redis 127.0.0.1:7000> set a aaa
QUEUED
redis 127.0.0.1:7000> set b bbb
QUEUED
redis 127.0.0.1:7000> set c ccc
QUEUED
redis 127.0.0.1:7000> exec
1) OK
2) OK
3) OK

*/
func Transsactions()  {

	c.Send("MULTI")
	c.Do("SET", "mul","alice")
	c.Do("APPEND", "mul","bob")
	c.Do("APPEND", "mul","cc")
	//c.Send("INCR", "foo")
	//c.Send("INCR", "bar")
	r, err := c.Do("EXEC")
	fmt.Println(r, err)

	r, err = redis.String(c.Do("GET", "mul"))

	fmt.Println("result", r)
}