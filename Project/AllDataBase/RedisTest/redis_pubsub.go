package RedisTest

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"myTool/common"
	"reflect"
	"time"
)

// 发布 订阅
//Redis 客户端可以订阅任意数量的频道。
/*
1.PSUBSCRIBE pattern [pattern ...]
订阅一个或多个符合给定模式的频道。
2	PUBSUB subcommand [argument [argument ...]]
查看订阅与发布系统状态。
3	PUBLISH channel message
将信息发送到指定的频道。
4	PUNSUBSCRIBE [pattern [pattern ...]]
退订所有给定模式的频道。
5	SUBSCRIBE channel [channel ...]
订阅给定的一个或多个频道的信息。
6	UNSUBSCRIBE [channel [channel ...]]
指退订给定的频道。

*/
func PubAndSub()  {


	c1,_ := Login()
	//监听test频道
	psc := redis.PubSubConn{Conn: c1}

	// 订阅 channel1 频道
	err:= psc.Subscribe("channel1")
	if err!=nil {
		fmt.Println("sub",err)
	}
	//订阅
	go func() {
		var receipt interface{}
		for  {
			receipt=psc.Receive()
			fmt.Println(reflect.ValueOf(receipt).String())
			if receipt !="" {
				switch v := receipt.(type) {
				case redis.Message://单个订阅subscribe
					fmt.Printf("%s: message: %s\n", v.Channel,v.Data)
				case redis.Subscription:
					fmt.Printf("Subscription: %s %s %d\n", v.Kind, v.Channel, v.Count)
					if v.Count == 0 {
						return
					}
				case error:
					fmt.Printf("error: %v\n", v)
					return

				}
			}
		}
	}()

	c2,_ := Login()
	//发送消息 到 频道 channel1
	for i:= 0;i < 10;i++ {
		time.Sleep(time.Second)
		_,err := c2.Do("PUBLISH","channel1","abc")
		common.CheckErr(err)
	}


}



