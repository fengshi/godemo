package RedisTest

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
)


/*
1	AUTH password
验证密码是否正确
2	ECHO message
打印字符串
3	PING
查看服务是否运行
4	QUIT
关闭当前连接
5	SELECT index
切换到指定的数据库
*/

func Login()(redis.Conn, error)  {

	//155.138.161.50
	//c, err := redis.Dial("tcp", "39.96.83.182:6379")
	//if err != nil {
	//	return nil, err
	//}
	//
	//if _, err := c.Do("AUTH", "Crysto.Redis.666"); err != nil {
	//	return nil, err
	//}
	//
	//return c, nil

	c, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		return nil, err
	}

	//if _, err := c.Do("AUTH", "pwd www.cosex.com2019"); err != nil {
	//	return nil, err
	//}

	return c, nil
}

func Ping()  {
	c, err := redis.Dial("tcp", "39.96.83.182:6379")
	if err != nil {
		panic(err)
	}
	r, err := c.Do("PING")

	fmt.Println("ping", r, err)


	c,err = Login()
	r, err = c.Do("PING")

	fmt.Println("ping2", r, err)
}

func LoginWithOptions()  {

	c, err := redis.Dial("tcp", "39.96.83.182:6379", redisOptions()...)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer c.Close()

	_, err = c.Do("SET", "mykey", "superWang")
	if err != nil {
		fmt.Println("redis set failed:", err)
	}

	username, err := redis.String(c.Do("GET", "mykey"))
	if err != nil {
		fmt.Println("redis get failed:", err)
	} else {
		fmt.Printf("Get mykey: %v \n", username)
	}

}


/*可选的参数
func DialConnectTimeout(d time.Duration) DialOption
func DialDatabase(db int) DialOption
func DialKeepAlive(d time.Duration) DialOption
func DialNetDial(dial func(network, addr string) (net.Conn, error)) DialOption
func DialPassword(password string) DialOption
func DialReadTimeout(d time.Duration) DialOption
func DialWriteTimeout(d time.Duration) DialOption
*/

func redisOptions() []redis.DialOption {
	cnop := redis.DialConnectTimeout(time.Second * 10)
	rdop := redis.DialReadTimeout(time.Second * 10)
	wrop := redis.DialWriteTimeout(time.Second * 10)
	auop := redis.DialPassword("qjn19920314")
	return []redis.DialOption{cnop, rdop, wrop, auop}
}

