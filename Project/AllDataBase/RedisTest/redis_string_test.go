package RedisTest

import "testing"

func TestSetAndGet(t *testing.T) {
	SetAndGet()
}

func TestGetset(t *testing.T) {
	Getset()
}

func TestMsetGet1(t *testing.T) {
	MsetGet1()
}


func TestMsetGet2(t *testing.T) {
	MsetGet2()
}

func TestSetNx(t *testing.T) {
	SetNx()
}

func TestMsetNx(t *testing.T) {
	MsetNx()
}

func TestEncr(t *testing.T) {
	Incr()
}

func TestIncrby(t *testing.T) {
	Incrby()
}

func TestDecr(t *testing.T) {
	Decr()
}

func TestDecrby(t *testing.T) {
	Decrby()
}

func TestAppend(t *testing.T) {
	Append()
}

func TestSubstr(t *testing.T) {
	Substr()
}

func TestStrlen(t *testing.T) {
	Strlen()
}