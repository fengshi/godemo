package parse

import (
	"bufio"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"goDemo/Project/movieCrawler/model"
	"os"
	"strconv"
	"strings"
)


var movieUrl = "https://movie.douban.com/subject/%s/"

var movieSep = ": "
func GetInfoWithID(ID string) *model.Movie  {

	//url := fmt.Sprintf(movieUrl, ID)
	//doc, err := tool.GetUrlDoc(url)
	url := "/Users/qianjianeng/Desktop/1.html"
	file, err := os.Open(url)


	doc, err := goquery.NewDocumentFromReader(bufio.NewReader(file))

	if err != nil {
		return nil
	}

	//var movie = model.Movie{}
	//doc.Find("div#info").Children().EachWithBreak(func(i int, s *goquery.Selection) bool {
	//
	//	director := s.Find("span > span.attrs > a").Eq(0).Text()
	//	fmt.Println(director)
	//
	//	//s.ParentFiltered("span > span.attrs").Each(func(i int, s *goquery.Selection) {
	//	//	bianju := s.Find("span > span.pl").Eq(1).Text()
	//	//	fmt.Println(bianju)
	//	//})
	//
	//	a := s.Find("span > span.pl").Eq(i).Text()
	//	if len(a) > 0 {
	//		fmt.Println(i,a)
	//	}
	//
	//	//s.Children().EachWithBreak(func(j int, selection *goquery.Selection) bool {
	//	//
	//	//
	//	//
	//	//
	//	//
	//	//
	//	//	return true
	//	//
	//	//})
	//	//if i == 0 {
	//	//	return false
	//	//}
	//	return true
	//
	//})


	var movie = model.Movie{}
	movie.ID, _ = strconv.Atoi(ID)

	// 标题 和 子标题
	// > div#interest_sectl > div.rating_wrap clearbox > div.rating_self clearfix > strong
	doc.Find("body > div#wrapper > div#content > div.grid-16-8 clearfix > div.article > div.indent clearfix").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("").Text()
		//fmt.Printf("Review %d: %s - %s\n", i, band)
		//
		//op, _ := s.Attr("property")
		//con, _ := s.Attr("content")
		//if op == "v:itemreviewed" {
		//	fmt.Println("------", con)
		//}

		fmt.Println("star", s.Text())
	})
	doc.Find("body > div#wrapper > div#content > h1 > span").Each(func(i int, s *goquery.Selection) {

		if i == 0 {
			title := s.Eq(0).Text()
			sp := strings.Split(title, " ")
			if len(sp) > 1 {
				movie.Title = sp[0]
				movie.Subtitle = strings.Join(sp[1:], " ")
			} else {
				movie.Title = title
			}
		}

		
	})
	fmt.Println("///",movie.Title, "//",movie.Subtitle)


	doc.Find("body > div#wrapper > div#article > div#rating_wrap clearbox > strong#ll rating_num").Each(func(i int, s *goquery.Selection) {

		fmt.Println(s.Text())
	})

	// 基本信息
	doc.Find("div#info").Each(func(i int, s *goquery.Selection) {
		//a := s.Find("span > span.pl").Eq(i).Text()
		//if len(a) > 0 {
		//	fmt.Println("--",i,a)
		//}

		// 基本信息
		/*
		导演: 约翰·李·汉考克
		编剧: 约翰·李·汉考克 / 约翰·福斯克
		主演: 凯文·科斯特纳 / 伍迪·哈里森 / 凯西·贝茨 / 金·迪肯斯 / 约翰·卡洛·林奇 / W·厄尔·布朗 / 托马斯·曼 / 大卫·福尔 / 杰西·C·博伊德 / 简·麦克尼尔 / 比利·斯洛特 / 大卫·德怀尔 / 杰森·戴维斯 / 乔希·卡拉斯 / 布莱恩·杜尔金 / 迪恩·J·韦斯特 / 大卫·博恩 / 迈克尔·德雷珀 / 杰夫·波普 / 托尼亚·马尔多纳多 / 库伦·莫斯 / 乔希·文图拉 / 迪安·丹顿 / 乔·奈泽维奇
		类型: 剧情 / 悬疑 / 惊悚 / 犯罪
		制片国家/地区: 美国
		语言: 英语
		上映日期: 2019-03-29(美国)
		片长: 132分钟
		又名: 追击雌雄大盗 / 缉狂公路(台) / 辣手骑警(港) / Highwaymen
		IMDb链接: tt1860242
		*/
		info := strings.Split(s.Text(),"\n")
		var result []string
		for _, v := range info {
			if len(strings.TrimSpace(v)) > 0 {
				result = append(result,strings.TrimSpace(v))
			}
		}

		if len(result) == 10 {
			movie.Director = strings.Split(result[0],movieSep)[1]
			movie.Scenarist = strings.Split(result[1],movieSep)[1]
			movie.Starring = strings.Split(result[2],movieSep)[1]
			movie.Type = strings.Split(result[3],movieSep)[1]
			movie.Area = strings.Split(result[4],movieSep)[1]
			movie.Language = strings.Split(result[5],movieSep)[1]
			movie.Year = strings.Split(result[6],movieSep)[1]
			movie.Duration = strings.Split(result[7],movieSep)[1]
			movie.Alias = strings.Split(result[8],movieSep)[1]
			movie.IMDbName = strings.Split(result[9],movieSep)[1]
		}

		for _, v := range result {
			fmt.Println(v)
		}

	})


	// 其他信息
	doc.Find("body > div#wrapper > div#content > h1 > span").Each(func(i int, s *goquery.Selection) {

		if i == 0 {
			title := s.Eq(0).Text()
			sp := strings.Split(title, " ")
			if len(sp) > 1 {
				movie.Title = sp[0]
				movie.Subtitle = strings.Join(sp[1:], " ")
			} else {
				movie.Title = title
			}
		}


	})

	//
	//
	//doc.Find("#content > div > div.article > ol > li").Each(func(i int, s *goquery.Selection) {
	//	title := s.Find(".hd a span").Eq(0).Text()
	//
	//	subtitle := s.Find(".hd a span").Eq(1).Text()
	//	subtitle = strings.TrimLeft(subtitle, "  / ")
	//
	//	alias := s.Find(".hd a span").Eq(2).Text()
	//	alias = strings.TrimLeft(alias, "  / ")
	//
	//	desc := strings.TrimSpace(s.Find(".bd p").Eq(0).Text())
	//	DescInfo := strings.Split(desc, "\n")
	//	desc = DescInfo[0]
	//
	//	movieDesc := strings.Split(DescInfo[1], "/")
	//	year := strings.TrimSpace(movieDesc[0])
	//	area := strings.TrimSpace(movieDesc[1])
	//	tag := strings.TrimSpace(movieDesc[2])
	//	tags := strings.Fields(tag)
	//
	//	star := s.Find(".bd .star .rating_num").Text()
	//
	//	comment := strings.TrimSpace(s.Find(".bd .star span").Eq(3).Text())
	//	compile := regexp.MustCompile("[0-9]")
	//	comment = strings.Join(compile.FindAllString(comment, -1), "")
	//
	//	quote := s.Find(".quote .inq").Text()
	//
	//	movie := model.Movie{
	//		Title:    title,
	//		Subtitle: subtitle,
	//		Alias:    alias,
	//		Desc:     desc,
	//		Year:     year,
	//		Area:     area,
	//		Tags:      tags,
	//		Star:     star,
	//		Comment:  comment,
	//		Quote:    quote,
	//	}
	//
	//	fmt.Println(movie)
	//})
	//
	//
	//



	return &model.Movie{}
}
