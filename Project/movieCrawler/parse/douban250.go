package parse

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"goDemo/Project/movieCrawler/model"
	"goDemo/Project/movieCrawler/tool"
	"log"
	"regexp"
	"strconv"
	"strings"
)





type Douban250 struct {

}

func NewDouban250() *Douban250   {
	return &Douban250{}
}

type Page struct {
	Page int
	Url  string
}

var baseurl = "https://movie.douban.com/top250"

func (eng *Douban250)Run()  {

	var movies []model.Movie

	pages := GetPages(baseurl)
	for _, page := range pages {

		doc, err := tool.GetUrlDoc(strings.Join([]string{baseurl, page.Url}, ""))
		if err != nil {
			log.Println(err)
		}

		movies = append(movies, ParseMovies(doc)...)
	}

	fmt.Println(movies)

}


// 获取分页
func GetPages(url string) []Page {
	doc, err := tool.GetUrlDoc(url)
	if err != nil {
		log.Fatal(err)
	}

	return ParsePages(doc)
}

// 分析分页
func ParsePages(doc *goquery.Document) (pages []Page) {
	pages = append(pages, Page{Page: 1, Url: ""})
	doc.Find("#content > div > div.article > div.paginator > a").Each(func(i int, s *goquery.Selection) {
		page, _ := strconv.Atoi(s.Text())
		url, _ := s.Attr("href")

		pages = append(pages, Page{
			Page: page,
			Url:  url,
		})
	})

	return pages
}

// 分析电影数据
func ParseMovies(doc *goquery.Document) (movies []model.Movie) {
	doc.Find("#content > div > div.article > ol > li").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".hd a span").Eq(0).Text()

		subtitle := s.Find(".hd a span").Eq(1).Text()
		subtitle = strings.TrimLeft(subtitle, "  / ")

		alias := s.Find(".hd a span").Eq(2).Text()
		alias = strings.TrimLeft(alias, "  / ")

		desc := strings.TrimSpace(s.Find(".bd p").Eq(0).Text())
		DescInfo := strings.Split(desc, "\n")
		desc = DescInfo[0]

		movieDesc := strings.Split(DescInfo[1], "/")
		year := strings.TrimSpace(movieDesc[0])
		area := strings.TrimSpace(movieDesc[1])
		tag := strings.TrimSpace(movieDesc[2])

		star := s.Find(".bd .star .rating_num").Text()

		comment := strings.TrimSpace(s.Find(".bd .star span").Eq(3).Text())
		compile := regexp.MustCompile("[0-9]")
		comment = strings.Join(compile.FindAllString(comment, -1), "")

		//quote := s.Find(".quote .inq").Text()

		movie := model.Movie{
			Title:    title,
			Subtitle: subtitle,
			Alias:    alias,
			Desc:     desc,
			Year:     year,
			Area:     area,
			Type:      tag,
			Star:     star,
		}

		log.Printf("i: %d, movie: %v", i, movie)

		movies = append(movies, movie)
	})

	return movies
}
