package tool

import (
	"github.com/PuerkitoBio/goquery"
	"goDemo/Project/movieCrawler/Http"
)

func GetUrlDoc(url string) (*goquery.Document, error) {

	reader, err := Http.Get(url)
	if err != nil {
		return nil, err
	}
	return goquery.NewDocumentFromReader(reader)

}
