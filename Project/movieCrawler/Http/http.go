package Http

import (
	"io"
	"net/http"
)

func Get(url string) (io.Reader, error)  {
	cli := http.Client{}
	resp, err := cli.Get(url)
	if err != nil {
		return nil, err
	}
	return resp.Body,  nil
}
