package model

/*
		导演: 约翰·李·汉考克
		编剧: 约翰·李·汉考克 / 约翰·福斯克
		主演: 凯文·科斯特纳 / 伍迪·哈里森 / 凯西·贝茨 / 金·迪肯斯 / 约翰·卡洛·林奇 / W·厄尔·布朗 / 托马斯·曼 / 大卫·福尔 / 杰西·C·博伊德 / 简·麦克尼尔 / 比利·斯洛特 / 大卫·德怀尔 / 杰森·戴维斯 / 乔希·卡拉斯 / 布莱恩·杜尔金 / 迪恩·J·韦斯特 / 大卫·博恩 / 迈克尔·德雷珀 / 杰夫·波普 / 托尼亚·马尔多纳多 / 库伦·莫斯 / 乔希·文图拉 / 迪安·丹顿 / 乔·奈泽维奇
		类型: 剧情 / 悬疑 / 惊悚 / 犯罪
		制片国家/地区: 美国
		语言: 英语
		上映日期: 2019-03-29(美国)
		片长: 132分钟
		又名: 追击雌雄大盗 / 缉狂公路(台) / 辣手骑警(港) / Highwaymen
		IMDb链接: tt1860242
*/
type Movie struct {
	ID        int
	Title     string //名称
	Subtitle  string //子名称
	Director  string //导演
	Scenarist string //编剧
	Starring  string //主演
	Type      string //标签 类型
	Area      string //国家 地区
	Language  string //语言
	Year      string //上映时间
	Duration  string //时长
	Alias     string //别名

	Posters []string //海报  url 用 && 连接
	Star    string   //评分
	Desc    string   //简介

	IMDbName string //IMDB显示名称
	IMDbUrl  string //IMDB链接
	Seed     string //迅雷种子
	similar  []Mov  //相关电影
}

type Mov struct {
	ID     string
	Title  string
	Poster string
}

type Page struct {
	Page int
	Url  string
}
