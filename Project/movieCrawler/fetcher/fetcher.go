package fetcher
import (
	"goDemo/Project/movieCrawler/parse"

)



type Fetcher struct {
	Douban *parse.Douban250
}

func NewFetcher() *Fetcher  {
	var f = Fetcher{Douban:parse.NewDouban250()}
	return &f
}

func (f *Fetcher)Run()  {
	f.Douban.Run()
}