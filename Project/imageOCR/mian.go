package main

import (
	"fmt"
	"github.com/otiai10/gosseract"
)

func main() {
	client := gosseract.NewClient()
	defer client.Close()
	url := "./data/002.png"
	url = "./data/001.png"
	err := client.SetImage(url)
	if err != nil  {
		fmt.Println(err)
	}
	text, err := client.Text()
	fmt.Println(text, err)
	// Hello, World!
}

