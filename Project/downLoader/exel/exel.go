package exel

import (
	"encoding/csv"
	"goDemo/Project/downLoader/util"
	"io/ioutil"
	"strings"
)

const vedioPrefix = "http://video.like"

//获取 url
func GetUrls() []string  {
	files := GetCSVpaths()
	var result []string
	for _,file := range files{
		urls := getUrlFromCsvFile(file)
		if len(urls) > 0 {
			result = append(result, urls...)
		}
	}
	return result
}

//获取csv 文件
func GetCSVpaths() []string  {
	path := util.GetCurrentDirectory() +"/Desktop/downLoader"+ "/config"
	files := getAllCSVFile(path)
	if len(files) == 0 {
		panic("config 目录下没有scv文件")
	}

	return files
}

func getAllCSVFile(directory string) []string  {

	files,err := util.GetAllFiles(directory)
	if err != nil {
		panic(err)
	}
	var results []string
	for _, file := range files {
		if strings.HasSuffix(file,".csv") {
			results = append(results, file)
		} else if strings.HasSuffix(file,".xls") {
			results = append(results, file)
		} else if strings.HasSuffix(file,".xlsx") {
			results = append(results, file)
		}
	}
	return results
}

//从csv中 读取url
func getUrlFromCsvFile(file string) []string  {
	cntb, err := ioutil.ReadFile(file)
	if err != nil {
		return nil
	}
	// 读取文件数据
	r2 := csv.NewReader(strings.NewReader(string(cntb)))
	ss, _ := r2.ReadAll()
	sz := len(ss)
	// 循环取数据
	var result []string
	for i := 0; i < sz; i++ {
		for _,v := range ss[i] {
			if strings.HasPrefix(v,vedioPrefix) {
				result = append(result,v)
			}
		}
		//fmt.Println(ss[i][3]) //  key的数据  可以作为map的数据的值
	}
	return result
}


//下载文件
func Download(url string) error {
	dir := util.DownloadDirectory()
	return util.Download(url,dir)
}