package loader

import (
	"fmt"
	"strconv"
	"time"
)

var TotalCount = 0 //需要下载的总数
var LoadCount = 0  //已经下载的数量

var strCount = 0 //下载个数的位数，用来前置补0
func GetFilename()string  {
	if strCount == 0 {
		strCount = len(strconv.Itoa(TotalCount))
	}
	day := "video_"
	index := LoadCount + 1
	//indexStr := fmt.Sprintf("%0*d", strCount, index)
	return day+ fmt.Sprintf("%v", index)
}

func getDayString() string {
	year, month, day := time.Now().UTC().Date()
	return fmt.Sprintf("%d%d%d-", year, int(month), day)
}
