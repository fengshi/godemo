package util

import (
	"bufio"
	"fmt"
	"goDemo/Project/downLoader/loader"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

const bufferSize  = 1024
// Download 下载功能, 第二个可选参数为一个文件夹名称
func Download(url string, Directory string) error {

	filename := loader.GetFilename()
	filename += ".mp4"
	filePath := filepath.Join(Directory, filename)

	if Exist(filePath) {
		return fmt.Errorf(fmt.Sprintln("文件已存在", url))
	}
	res,  err := http.Get(url)
	if err != nil{
		return err
	}
	defer res.Body.Close()

	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	defer file.Close()
	if err != nil {
		fmt.Println("open log file err", err)
		return err
	}
	w := bufio.NewWriterSize(file, bufferSize)
	defer w.Flush()

	result, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	_ , err = w.Write(result)
	return err
}
