package main

import (
	"fmt"
	"goDemo/Project/downLoader/exel"
	"goDemo/Project/downLoader/loader"
	"sync"
)

func main() {

	fmt.Println("支持的下载文件格式有：(.csv) ")
	urls := exel.GetUrls()
	var wg sync.WaitGroup

	loader.TotalCount = len(urls)
	wg.Add(loader.TotalCount)
	fmt.Printf("一共有链接  %d 个 \n", loader.TotalCount)
	loader.LoadCount = 0
	for i := 0;i < loader.TotalCount;i++ {
		err := exel.Download(urls[i])
		if err == nil {
			loader.LoadCount ++
		}
		wg.Done()
		if loader.LoadCount % 20 == 0 {
			fmt.Printf("已经下载了  %d 个，还有 %d 个 \n", loader.LoadCount,loader.TotalCount-loader.LoadCount)
		}
	}
	wg.Wait()
	fmt.Printf("成功下载  %d 个 \n", loader.LoadCount)
}
