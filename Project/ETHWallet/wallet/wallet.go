package wallet

import (
	"github.com/miguelmota/go-ethereum-hdwallet"
	"github.com/tyler-smith/go-bip39"
)

type Wallet struct {
	Mnemonic string
	PrivateKey string
	Address   string
}

func NewWallet() *Wallet {

	entropy, _ := bip39.NewEntropy(128)
	mnemonic, _ := bip39.NewMnemonic(entropy)

	wallet, _ := hdwallet.NewFromMnemonic(mnemonic)

	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, _ := wallet.Derive(path, false)

	//fmt.Printf("Account address: %s\n", account.Address.Hex())

	privateKey, _ := wallet.PrivateKeyHex(account)

	//fmt.Printf("Private key in hex: %s\n", privateKey)

	wa := Wallet{}
	wa.Mnemonic = mnemonic
	wa.PrivateKey = privateKey
	wa.Address = account.Address.Hex()
	return &wa

}