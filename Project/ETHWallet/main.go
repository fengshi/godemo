package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goDemo/Project/ETHWallet/balance"
	"goDemo/Project/ETHWallet/email"
	"goDemo/Project/ETHWallet/wallet"
	"time"
)

var liveing bool
func main() {

	go liveCheck()

	quit := make(chan int)
	email.SendEmail("邮件 -- 测试")

	go work(quit)
	fmt.Println("程序运行中......")

	var ticker = time.NewTicker(time.Minute * 30)
	for range ticker.C {
		fmt.Printf("trying count %d \n", balance.Count)

		if time.Now().Hour() >= 10 || time.Now().Hour() <= 23 {
			if len(quit) > 0 {
				quit <- 1
			}
		} else {
			if liveing == false {
				go work(quit)
			}
		}

	}

}

func work(quit <-chan int) {

	for {
		select {
		case <-quit:
			fmt.Println("cancel goroutine by channel!")
			liveing = false
			return
		default:
			wa := wallet.NewWallet()
			balance.GetBalance(wa)
			liveing = true
		}
	}

}

func liveCheck()  {
	r := gin.Default()
	r.GET("/live", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"message": "success",
			"count":balance.Count,
		})
	})

	err := r.Run(":9999")
	if err != nil {
		panic(err)
	}
}