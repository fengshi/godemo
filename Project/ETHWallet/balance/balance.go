package balance

import (
	"encoding/json"
	"fmt"
	"goDemo/Project/ETHWallet/tool"
	"goDemo/Project/ETHWallet/wallet"
	"io/ioutil"
	"net/http"
	"strconv"
)

var BaseUrl = "https://api.etherscan.io/api?module=account&action=balance"

var Count = 0


type balance struct {
	Status string
	Message string
	Result string
} 

func GetBalance(wallet *wallet.Wallet)  {

	if len(wallet.PrivateKey) == 0 || len(wallet.Address) == 0 {
		return
	}

	url := fmt.Sprintf("%s&address=%s&tag=latest&apikey=eth", BaseUrl, wallet.Address)

	bal := httpGetBalance(url)

	value,_ := strconv.Atoi(bal.Result)
	if bal.Status == "1" && value > 0 {
		tool.WriteDown(wallet)
		tool.SendToEmail(wallet)
	}

}

func httpGetBalance(url string) balance   {
	Count += 1
	client := http.Client{}
	resp, err := client.Get(url)

	if err != nil {
		return balance{}
	} else {
		defer resp.Body.Close()
		result, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return balance{}
		}
		b := balance{}

		json.Unmarshal(result, &b)
		return b
	}
}