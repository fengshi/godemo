package main

import (
	"fmt"
	"goDemo/Project/redis/red"
)

func main() {

	//fmt.Println("------ SetGet ---------")
	//red.SetGet()
	//
	//fmt.Println("------ SetGetWithExpired ---------")
	//red.SetGetWithExpired()
	//
	//fmt.Println("------ Append ---------")
	//red.Append()

	//fmt.Println("------ SetGetBatch ---------")
	//red.SetGetBatch()
	//
	//fmt.Println("------ SetGetBatch2 ---------")
	//red.SetGetBatch2()

	fmt.Println("------ SetGetObjectBatch ---------")
	red.SetGetObjectBatch()

}
