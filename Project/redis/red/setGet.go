package red

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"myTool/common"
	"time"
)

// 简单存取
func SetGet() {

	c, err := Login()
	defer c.Close()
	if err == nil {
		//对本次连接进行set操作
		_, setErr := c.Do("set", "url", "xx.github.io")

		common.CheckErr(setErr)
		//使用redis的string类型获取set的k/v信息
		r, getErr := redis.String(c.Do("get", "url"))
		common.CheckErr(getErr)

		fmt.Println(r)
	} else {
		fmt.Println(err)
	}

}

// 设置过期
//EX seconds -- 指定过期时间，单位为秒.
//PX milliseconds -- 指定过期时间，单位为毫秒.
//NX -- 仅当键值不存在时设置键值.
//XX -- 仅当键值存在时设置键值.
func SetGetWithExpired() {
	c, err := Login()
	defer c.Close()
	if err == nil {
		//设置过期时间 （过期时间单位 秒）
		_, setErr := c.Do("SET", "expired", "xx.github.io", "EX", 2)
		common.CheckErr(setErr)

		time.Sleep(3 * time.Second)
		r, getErr := redis.String(c.Do("GET", "expired"))
		common.CheckErr(getErr)

		fmt.Println(r)
	} else {
		fmt.Println(err)
	}
}

// 在旧的value 上追加 value
func Append() {
	c, err := Login()
	defer c.Close()
	if err == nil {
		_, setErr := c.Do("APPEND", "name", "zhangsan")
		common.CheckErr(setErr)

		r, getErr := redis.String(c.Do("GET", "name"))
		common.CheckErr(getErr)

		fmt.Println(r)
	} else {
		fmt.Println(err)
	}

}

//批量读写
func SetGetBatch() {
	c, err := Login()
	defer c.Close()
	if err == nil {
		//设置过期时间 （过期时间单位 秒）

		// 格式 kvkvkvkv

		var keys []interface{}
		var values []interface{}
		for i := 0; i < 10; i++ {
			keys = append(keys, i)
			values = append(values, i, fmt.Sprintf("--%v==", i))
		}

		fmt.Println(values, len(values))
		err := c.Send("MSET", values...)
		common.CheckErr(err)


		r,getErr := redis.Strings(c.Do("MGET", keys...))
		common.CheckErr(getErr)

		fmt.Println(r)
	} else {
		fmt.Println(err)
	}
}

//批量读写
func SetGetBatch2() {
	c, err := Login()
	defer c.Close()
	if err == nil {
		//设置过期时间 （过期时间单位 秒）

		// 格式 kvkvkvkv

		var keys []interface{}
		values := redis.Args{}
		for i := 10; i < 20; i++ {
			keys = append(keys, i)
			values = values.Add(i).Add(fmt.Sprintf("--%v==", i))
		}

		fmt.Println(values, len(values))
		err := c.Send("MSET", values...)
		common.CheckErr(err)


		r,getErr := redis.Strings(c.Do("MGET", keys...))
		common.CheckErr(getErr)

		fmt.Println(r)
	} else {
		fmt.Println(err)
	}
}


