package red

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"reflect"
)


/*
 每个hash 可以存储 232 - 1 键值对
hset(key, field, value)：向名称为key的hash中添加元素field
hget(key, field)：返回名称为key的hash中field对应的value

hmget(key, (fields))：返回名称为key的hash中field i对应的value
hmset(key, (fields))：向名称为key的hash中添加元素field

hincrby(key, field, integer)：将名称为key的hash中field的value增加integer

hexists(key, field)：名称为key的hash中是否存在键为field的域
hdel(key, field)：删除名称为key的hash中键为field的域
hlen(key)：返回名称为key的hash中元素个数
hkeys(key)：返回名称为key的hash中所有键
hvals(key)：返回名称为key的hash中所有键对应的value
hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value
---------------------

*/


/*
	c, err := Login()
	defer c.Close()
	if err == nil {

	}
*/
func HsetHget()  {
	c, err := Login()
	defer c.Close()
	if err == nil {
		_, err = c.Do("hset", "hsethget-myhash", "bike1", []byte("mobike"),"bike2","blue")
		if err != nil {
			fmt.Println("haset failed", err.Error())
		}
	}

	res, err := c.Do("hget", "hsethget-myhash", "bike1")
	fmt.Println(reflect.TypeOf(res))
	if err != nil {
		fmt.Println("hget failed", err.Error())
	} else {
		fmt.Printf("hget value : %s\n", res.([]byte))
	}

	res, err = c.Do("hget", "hsethget-myhash", "bike2")
	fmt.Println(reflect.TypeOf(res))
	if err != nil {
		fmt.Println("hget failed", err.Error())
	} else {
		fmt.Printf("hget value2 : %s\n", res.([]byte))
	}


}


// 批量读写对象
type Person struct {
	Id int
	Name string
	Age int
	Weight float32
}

func createPersons() []Person  {

	p1 := Person{1,"Alice", 12, 40.5}
	p2 := Person{2,"Bob", 42, 46.5}
	p3 := Person{3,"Cily", 32, 70.2}
	p4 := Person{4,"Didty", 17, 30.5}
	p5 := Person{5,"Edwrd", 40, 60.7}

	ps := []Person{p1,p2,p3,p4,p5}
	return ps

}

func HmsetHmget1()  {
	c, err := Login()
	defer c.Close()
	if err == nil {
		_, err = c.Do("hmset", "hmsg-myhash","bike1", "mobai", "bike2", "bluegogo", "bike3", "xiaoming", "bike4", "xiaolan")
		if err != nil {
			fmt.Println("hmset error", err.Error())
		} else {
			value, err := redis.Values(c.Do("hmget", "hmsg-myhash", "bike1", "bike2", "bike3", "bike4"))
			if err != nil {
				fmt.Println("hmget failed", err.Error())
			} else {
				fmt.Printf("hmget myhash's element : \n")
				for index, v := range value {
					fmt.Printf("%v %s \n", index, v.([]byte))
				}
				fmt.Printf("\n")
			}
		}
	}
}

// 存储对象
func HmsetHmget2()  {
	c, err := Login()
	defer c.Close()

	pers := createPersons()
	if err == nil {
		if _, err := c.Do("HMSET", redis.Args{}.Add("p1").AddFlat(&pers[0])...); err != nil {
			panic(err)
		}

		m := map[string]string{
			"Id":  "6",
			"Name": "Steve",
			"Age":   "22",
			"Weight": "50.6",
		}

		if _, err := c.Do("HMSET", redis.Args{}.Add("p2").AddFlat(m)...); err != nil {
			panic(err)
		}

		var person Person
		for _, id := range []string{"p1", "p2"} {

			v, err := redis.Values(c.Do("HGETALL", id))
			if err != nil {
				panic(err)
			}

			if err := redis.ScanStruct(v, &person); err != nil {
				panic(err)
			}

			fmt.Printf("%+v\n", person)
		}
		
	}
}

func Hincrby()  {

	c, err := Login()
	defer c.Close()
	if err == nil {
		_, err = c.Do("hmset", "Hincrby-myhash", "bike2", "bluegogo", "bike3", "xiaoming", "bike4", "xiaolan")
		if err != nil {
			fmt.Println("hmset error", err.Error())
		} else {
			value, err := redis.Values(c.Do("hmget", "Hincrby-myhash", "bike1", "bike2", "bike3", "bike4"))
			if err != nil {
				fmt.Println("hmget failed", err.Error())
			} else {
				fmt.Printf("hmget myhash's element :")
				for _, v := range value {
					fmt.Printf("%s ", v.([]byte))
				}
				fmt.Printf("\n")
			}
		}
	}



}