package red
import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
)


func Login()(redis.Conn, error)  {

	//155.138.161.50
	c, err := redis.Dial("tcp", "127.0.0.1:6379")
	if err != nil {
		return nil, err
	}

	if _, err := c.Do("AUTH", "qjn19920314"); err != nil {
		return nil, err
	}

	return c, nil
}

func LoginWithPass()  {

	c, err := redis.Dial("tcp", "155.138.161.50:6379", redisOptions()...)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer c.Close()

	_, err = c.Do("SET", "mykey", "superWang")
	if err != nil {
		fmt.Println("redis set failed:", err)
	}

	username, err := redis.String(c.Do("GET", "mykey"))
	if err != nil {
		fmt.Println("redis get failed:", err)
	} else {
		fmt.Printf("Get mykey: %v \n", username)
	}

}


/*可选的参数
func DialConnectTimeout(d time.Duration) DialOption
func DialDatabase(db int) DialOption
func DialKeepAlive(d time.Duration) DialOption
func DialNetDial(dial func(network, addr string) (net.Conn, error)) DialOption
func DialPassword(password string) DialOption
func DialReadTimeout(d time.Duration) DialOption
func DialWriteTimeout(d time.Duration) DialOption
*/

func redisOptions() []redis.DialOption {
	cnop := redis.DialConnectTimeout(time.Second * 10)
	rdop := redis.DialReadTimeout(time.Second * 10)
	wrop := redis.DialWriteTimeout(time.Second * 10)
	auop := redis.DialPassword("qjn19920314")
	return []redis.DialOption{cnop, rdop, wrop, auop}
}
