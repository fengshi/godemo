package red

//管道化

/*
请求/响应服务可以实现持续处理新请求，即使客户端没有准备好读取旧响应。这样客户端可以发送多个命令到服务器而无需等待响应，最后在一次读取多个响应。这就是管道化(pipelining)
连接支持使用Send()，Flush()，Receive()方法支持管道化操作
Send(commandName string, args ...interface{}) error
Flush() error
Receive() (reply interface{}, err error)
*/


