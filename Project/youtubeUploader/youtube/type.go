package youtube

import (
	"google.golang.org/api/googleapi"
	)

//filename          = flag.String("filename", "", "Filename to upload. Can be a URL")
//thumbnail         = flag.String("thumbnail", "", "Thumbnail to upload. Can be a URL")
//caption           = flag.String("caption", "", "Caption to upload. Can be URL")
//title             = flag.String("title", "Video Title", "Video title")
//description       = flag.String("description", "uploaded by youtubeuploader", "Video description")
//language          = flag.String("language", "en", "Video language")
//categoryId        = flag.String("categoryId", "", "Video category Id")
//tags              = flag.String("tags", "", "Comma separated list of video tags")
//privacy           = flag.String("privacy", "public", "Video privacy status")//private  public unlisted
//quiet             = flag.Bool("quiet", false, "Suppress progress indicator")
//rate              = flag.Int("ratelimit", 0, "Rate limit upload in kbps. No limit by default")
//metaJSON          = flag.String("metaJSON", "", "JSON file containing title,description,tags etc (optional)")
//limitBetween      = flag.String("limitBetween", "", "Only rate limit between these times e.g. 10:00-14:00 (local time zone)")
//headlessAuth      = flag.Bool("headlessAuth", false, "set this if no browser available for the oauth authorisation step")
//oAuthPort         = flag.Int("oAuthPort", 8080, "TCP port to listen on when requesting an oAuth token")
//chunksize         = flag.Int("chunksize", googleapi.DefaultUploadChunkSize, "size (in bytes) of each upload chunk. A zero value will cause all data to be uploaded in a single request")
//notifySubscribers = flag.Bool("notify", true, "notify channel subscribers of new video")

type youConfig struct {
	Filename string
	Thumbnail	string
	Caption	string	//字慕
	Title string
	Description string
	Language string
	CategoryId string
	Tags []string
	Privacy string
	Quiet bool
	Rate int
	MetaJSON string
	LimitBetween string
	HeadlessAuth bool
	OAuthPort int
	Chunksize int
	NotifySubscribers bool
	SecrectFile string
}

func NewYouConfig(filepath,title,desc string,tags[]string,categoryId string,secret string) youConfig  {

	you := youConfig{}
	you.Filename = filepath
	you.Title = title
	you.Description = desc
	you.Language = "en"
	you.CategoryId = categoryId
	you.Tags = tags
	you.Privacy = "public"
	you.HeadlessAuth = false
	you.OAuthPort = 8080
	you.Chunksize = googleapi.DefaultUploadChunkSize
	you.NotifySubscribers = true

	you.SecrectFile = secret

	return you

}