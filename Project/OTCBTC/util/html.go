package util

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"strconv"
	"strings"
)

const OTC_USDT_URL  = "https://otcbtc.io/sell_offers?currency=usdt&fiat_currency=cny&payment_type=all"

const OTC_ADA_URL = "https://otcbtc.io/sell_offers?currency=ada&fiat_currency=cny&payment_type=all"

func USDTBalance() (value float64) {

	res, err := http.Get(OTC_USDT_URL)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return
	}

	// Load the HTML document
	doc, _ := goquery.NewDocumentFromReader(res.Body)
	// Find the review items

	min := 10.0
	doc.Find(".recommend-card__price").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("").Text()
		//fmt.Printf("Review %d: %s - %s\n", i, band)
		str := s.Text()
		value, err := strconv.ParseFloat(strings.TrimSpace(str), 64)
		if err == nil {
			if value < min {
				min = value
			}
		}

	})

	value = min
	return
}

func ADABalance() (value float64) {

	res, err := http.Get(OTC_ADA_URL)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return
	}

	// Load the HTML document
	doc, _ := goquery.NewDocumentFromReader(res.Body)
	// Find the review items

	min := 10.0
	doc.Find(".recommend-card__price").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("").Text()
		//fmt.Printf("Review %d: %s - %s\n", i, band)
		str := s.Text()
		value, err := strconv.ParseFloat(strings.TrimSpace(str), 64)
		if err == nil {
			if value < min {
				min = value
			}
		}

	})

	value = min
	return
}
func Vmate()  {
	url := "http://share007.vmate.in/hashtag_video_detail?app=officialsite&appver=1.12&chid=com.uc.vmate.app.ios&click_event=jrm6c6yt&id=a3q5rcz0xf5&la=en-in&open=0&src=flow"
	res, err := http.Get(url)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return
	}

	// Load the HTML document
	doc, _ := goquery.NewDocumentFromReader(res.Body)
	// Find the review items

	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("").Text()
		//fmt.Printf("Review %d: %s - %s\n", i, band)

		op, _ := s.Attr("property")
		con, _ := s.Attr("content")
		if op == "og:video" {
			fmt.Println("----",con)
		}
	})


}
