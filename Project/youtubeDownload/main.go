package main

import (
	"fmt"
	"goDemo/Project/youtubeDownload/youtube"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	//https://www.youtube.com/watch?v=KJFT_DMTjIk&list=PLddnd1SP38POfij8te6RKLpCS-LzAmRXf

	url := os.Args[1]

	fmt.Println(url)

	// 下载单个文件
	if strings.HasPrefix(url, "http") {
		youtube.DownLoadUrl(url)
	} else { // 下载多个文件
		buffer, err := ioutil.ReadFile(url)
		if err == nil {
			str := string(buffer)
			urls := strings.Split(str, "\n")

			for _, url := range urls {
				youtube.DownLoadUrl(url)
			}
		}
	}


}
