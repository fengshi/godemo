package youtube

import (
	"fmt"
	"github.com/rylio/ytdl"
	"goDemo/Project/videoBanyun/ffmpeg"
	"os"
)

func DownLoadUrl(url string) {
	vid, err := ytdl.GetVideoInfo(url)
	if err != nil {
		fmt.Println("Failed to get video info")

		return
	}
	desktop := os.ExpandEnv("$HOME") + "/DeskTop/"
	path := desktop + vid.Title + ".mp4"
	file, _ := os.Create(path)
	defer file.Close()
	err = vid.Download(vid.Formats[0], file)

	if err == nil {
		//判断格式
		info, _ := ffmpeg.GetVideoInfo(path)

		if info.Format != "mp4" {
			ffmpeg.CoverToMp4(path)
		}
	}

}

func DownloadUrlWithPath(url, path string) string{
	vid, err := ytdl.GetVideoInfo(url)
	if err != nil {
		fmt.Println("Failed to get video info")

		return ""
	}
	file, _ := os.Create(path)
	defer file.Close()
	err = vid.Download(vid.Formats[0], file)

	if err == nil {
		//判断格式
		info, _ := ffmpeg.GetVideoInfo(path)

		if info.Format != "mp4" {
			return ffmpeg.CoverToMp4(path)
		}
	}

	return path
}
