package mLog

import (
	"fmt"
	"testing"
)

func TestLog(t *testing.T) {
	Log(LDebug, "bug")
	Log(LInfo, "info")
	Log(LWarn, "warn")
	Log(LError, "error")
	Log(LFatal, "fatal")

	LogDebug("default")
	LogError("errors ")
}

func TestCallInfo(t *testing.T) {
	fmt.Println(getCallInfo())
}

func TestWriteToLogFile(t *testing.T) {
	writeToLogFile("test log file")
}

func TestLogFileName(t *testing.T) {
	fmt.Println(LogFileName())
}

func TestPrintln(t *testing.T) {
	Println("adsfafsas")
}
