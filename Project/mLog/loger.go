package mLog

type Outputer interface {
	Config(cfg Config)
	Output(level int, msg ...interface{})
}

const (
	LDebug = 1
	LInfo  = 2
	LWarn  = 3
	LError = 4
	LFatal = 5
)

//public
func Log(level int, msg ...interface{}) {

	for _, out := range outs {
		out.Output(level, msg...)
	}

}

func LogDebug(msg ...interface{}) {

	for _, out := range outs {
		out.Output(LDebug, msg...)
	}

}
func LogInfo(msg ...interface{}) {
	for _, out := range outs {
		out.Output(LInfo, msg...)
	}
}
func LogError(msg ...interface{}) {
	for _, out := range outs {
		out.Output(LError, msg...)
	}
}

func NewOutput(cfg Config) Outputer {
	var out Outputer
	switch cfg.Type {
	case 1:
		out = &ConsoleOutput{cfg}
	case 2:
		out = &FileOutput{cfg}

	}
	out.Config(cfg)
	return out
}

var outs []Outputer

func InitConfig(path string) {
	readConfig(path)
	cfgs := GetConfigs()
	for _, c := range cfgs {
		if c.Switch == 1 {
			output := NewOutput(c)
			outs = append(outs, output)
		}

	}
}
