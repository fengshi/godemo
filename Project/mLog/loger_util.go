package mLog

import (
	"fmt"
	"mcw/blockChain/common/util"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

//日志前缀信息
func LogPrefix() string {

	arr := make([]string, 0)
	// 1. 时间
	time := time.Now()
	timeStr := time.Format("2006/1/2 15:04:05")
	arr = append(arr, timeStr)
	// 2. 路径,行号
	_, file, line, _ := runtime.Caller(2)
	arr = append(arr, file)
	arr = append(arr, strconv.Itoa(line))

	return strings.Join(arr, "-") + "-"

}

//日志文件名
func LogFileName() string {
	now := time.Now().Format("20060102")
	return fmt.Sprintf("%v/log_%v.txt", logDir(), now)
}

func init() {

	if mcw_util.FileExists(logDir()) == false {
		err := os.MkdirAll(logDir(), os.ModePerm)
		if err != nil {
			panic("create logDir error:" + err.Error())
		}
	}
}

func logDir() string {
	dir, err := mcw_util.HomeDir()
	if err != nil {
		panic("get homedir error")
	}
	return dir + "/" + cfg.Dir.Name + "_log"
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

func HomeDir() (string, error) {
	user, err := user.Current()
	if nil == err {
		return user.HomeDir, nil
	}

	// cross compile support

	if "windows" == runtime.GOOS {
		return homeWindows()
	}

	// Unix-like system, so just assume Unix
	return homeUnix()
}

func homeUnix() (string, error) {
	// First prefer the HOME environmental variable
	if home := os.Getenv("HOME"); home != "" {
		return home, nil
	}

	// If that fails, try the shell
	var stdout bytes.Buffer
	cmd := exec.Command("sh", "-c", "eval echo ~$USER")
	cmd.Stdout = &stdout
	if err := cmd.Run(); err != nil {
		return "", err
	}

	result := strings.TrimSpace(stdout.String())
	if result == "" {
		return "", errors.New("blank output when reading home directory")
	}

	return result, nil
}

func homeWindows() (string, error) {
	drive := os.Getenv("HOMEDRIVE")
	path := os.Getenv("HOMEPATH")
	home := drive + path
	if drive == "" || path == "" {
		home = os.Getenv("USERPROFILE")
	}
	if home == "" {
		return "", errors.New("HOMEDRIVE, HOMEPATH, and USERPROFILE are blank")
	}

	return home, nil
}
