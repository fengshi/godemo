package mLog

import (
	"bufio"
	"fmt"
	"mcw/blockChain/common/util"
	"os"
)

var file FileOutput

type FileOutput struct {
	Cfg Config
}

// 日志写入文件缓存大小
var bufferSize = 500

func (p *FileOutput) Config(cfg Config) {
	file = FileOutput{cfg}
}

func (p *FileOutput) Output(level int, msg ...interface{}) {
	if level >= file.Cfg.Level {
		writeToLogFile(msg...)
	}

}

func writeToLogFile(msg ...interface{}) {

	fileName := LogFileName()
	pre := ""
	if mcw_util.FileExists(fileName) {
		pre = "\n"
	}
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	defer file.Close()
	if err != nil {
		Println("open log file err", err)
	}
	w := bufio.NewWriterSize(file, bufferSize)
	defer w.Flush()

	w.WriteString(fmt.Sprintf("%v%v%s", pre, LogPrefix(), msg))
}
