package mLog

import (
	"github.com/BurntSushi/toml"
)

var cfg ConfigLog
var confs []Config

type Config struct {
	Type   int
	Level  int
	Switch int
}

type DirConfig struct {
	Name string
}

type ConfigLog struct {
	Confs map[string]Config `toml:"config"`
	Dir   DirConfig
}

func GetConfigs() []Config {

	return confs
}

func readConfig(path string) {
	if _, err := toml.DecodeFile(path, &cfg); err != nil {
		panic(err)
	}
	LogDebug("log config data:", cfg)
	for _, v := range cfg.Confs {
		confs = append(confs, v)
	}
}
