package mLog

import (
	"fmt"
	"log"
	"runtime"
	"strings"
)

type ConsoleOutput struct {
	Cfg Config
}

func (p *ConsoleOutput) Config(cfg Config) {

}

func (p *ConsoleOutput) Output(level int, msg ...interface{}) {
	Println(msg)
}

func Println(a ...interface{}) {
	args := make([]interface{}, 0)
	args = append(args, getCallInfo())
	args = append(args, a...)
	log.Println(args...)
}

func Printf(format string, a ...interface{}) {
	args := make([]interface{}, 0)
	args = append(args, getCallInfo())
	args = append(args, a...)
	log.Printf("%s"+format, args...)
}

func getCallInfo() string {
	pc, file, line, _ := runtime.Caller(2)
	//str = fmt.Sprintf("测试一下输出颜色", 0x1B, 1, 46, 31, 0x1B)
	return fmt.Sprintf("%c[%d;%d;%dm[%s:%d: %s]%c[0m", 0x1B, 1, 46, 31, file[strings.LastIndexAny(file, "mcw")-6:len(file)],
		line, runtime.FuncForPC(pc).Name(), 0x1B)
}

func Error(err error) error {

	if err != nil {
		log.Println(fmt.Sprintf("%+v", err))
	}
	return err
}

func DLogln(a ...interface{}) {
	log.Println(getCallInfo(), a)
}

func DLog(format string, a ...interface{}) {
	log.Printf(getCallInfo()+format, a)
}
