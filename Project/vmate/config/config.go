package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"mcw/blockChain/common/util"
	"os"
)

var VamteConfig VamteCon

type VamteCon struct {
	Name string `toml:"name"`
	Vmate Vamte	`toml:"vmate"`
}

type Vamte struct {
	Top int64 `toml:"top"`
	TagLimit int64  `toml:"tagLimit"`
	TagSize int64	`toml:"tagSize"`
	TagCount int64	`toml:"tagCount"`
	LocalTags []string	`toml:"localTags"`
}

//func init() {
//
//	if _, err := toml.DecodeFile(ConfigPath(), &VamteConfig); err != nil {
//		panic(err)
//	}
//	mcw_logs.LogDebug("api config data", VamteConfig)
//
//}

func LoadConfig()VamteCon  {
	if _, err := toml.DecodeFile(ConfigPath(), &VamteConfig); err != nil {
		panic(err)
	}
	fmt.Println("配置文件信息：", VamteConfig)
	return VamteConfig

}

func ConfigPath() string {
	conPath := mcw_util.GetCurrentDirectory() + "/config/config.toml"
	if mcw_util.FileExists(conPath) == true {
		return conPath
	}

	gopath := os.Getenv("$HOME")
	//if len(gopath) ==0 {
	//	gopath = "/Users/qianjianeng"
	//}
	if len(gopath) ==0 {
		gopath = "/Users/lw"
	}
	absolutePath := gopath + "/Desktop/vmate/config/config.toml"
	if mcw_util.FileExists(absolutePath) {
		return absolutePath
	}
	pp := mcw_util.GetCurrentDirectory()+"/Desktop/vmate/config/config.toml"
	if mcw_util.FileExists(pp) {
		return pp
	}


	panic("api configfile is not exist")

}