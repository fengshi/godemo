package util

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"goDemo/Project/vmate/config"
	"net/http"
	"sort"
	"strconv"
)

const TagGroup = 500

//单个视频网页  a5sebw2w5qp
var videoweb = "https://www.vmate.com/hashtag_video_detail?app=officialsite&click_event=jsfiuvam&id=%v&open=0&src=flow"


// 需要tag 和 pageindes
var tagVides = "https://www.vmate.com/api/getHashTagFeed?id=%v&open=0&page=%v&pagesize=10"
//https://www.vmate.com/api/getHashTagFeed?id=holihai&open=0&page=0&pagesize=10


func MakeVedioUrsl()[]UrlModel  {

	
	con := config.LoadConfig()

	var videoursl []UrlModel
	tags := GetTags(con)

	fmt.Printf("\n符合条件的tag一共有 %v 个\n", len(tags))
	fmt.Println("开始拉取tag下的视频...")


	var videos []VideoModel
	for _, tag := range tags {

		v := GetVideoWithTag(tag)

		//单个tag下 数量超出限制，按点赞排序，取前面
		if int64(len(v)) > con.Vmate.TagCount {
			sort.Sort(ByVideoModelsLikeNum{v})
			v = v[:con.Vmate.TagCount]

			//for _,t := range v {
			//	fmt.Println(t.LikeNum)
			//}

		}
		videos = append(videos,v...)

	}


	//按点赞数从大到小排序
	for i := 0; i < len(videos); i++ {
		for j := 0; j < len(videos)-1; j++ {

			if videos[j].LikeNum < videos[j+1].LikeNum {
				videos[j].LikeNum, videos[j+1].LikeNum = videos[j+1].LikeNum, videos[j].LikeNum
			}
		}
	}

	//var loadVideoID []string

	//totalVideoCount := len(videos)
	//if con.Vmate.Top * 4 <  int64(totalVideoCount){
	//	videos = videos[:con.Vmate.Top * 4-1]
	//}

	//for i := 0; i < len(videos); i++ {
	//	//fmt.Println("视频点赞数：", videos[i].LikeNum)
	//	loadVideoID = append(loadVideoID, videos[i].ID)
	//}


	// 根据ID去重
	//loadVideoID = RemoveRep(loadVideoID)

	// map  ID -> tag
	var IDTag = make(map[string]string)
	for _, v := range videos {
		IDTag[v.ID] = v.Tag
	}


	// 根据点赞数目取前多少个
	//if len(loadVideoID) > int(con.Vmate.Top + 1) {
	//	loadVideoID = loadVideoID[:con.Vmate.Top]
	//}


	for k, v := range IDTag {
		videoweb := fmt.Sprintf(videoweb,k)
		url := GetVideoUrlFromweb(videoweb)
		res, err := HttpGetOri(url)
		if err == nil {
			defer res.Body.Close()
			value := res.Header.Get("Content-Length")
			if len(value) > 0  {
				size, err := strconv.Atoi(value)
				if err == nil && int64(size) > con.Vmate.TagSize {
					um := NewUrlModel(url, v)
					videoursl = append(videoursl,*um)
				}
			}

		}

	}

	//for i := 0; i < len(loadVideoID); i++ {
	//
	//	videoweb := fmt.Sprintf(videoweb,loadVideoID[i])
	//	url := GetVideoUrlFromweb(videoweb)
	//	// 这个url 是最终的视频的url
	//
	//	// check 视频大小是否符合要求
	//
	//	res, err := HttpGetOri(url)
	//	if err == nil {
	//		value := res.Header.Get("Content-Length")
	//		if len(value) > 0  {
	//			size, err := strconv.Atoi(value)
	//			if err == nil && int64(size) > con.Vmate.TagSize {
	//				um := NewUrlModel(url, )
	//			}
	//		}
	//
	//	}
	//
	//	videoursl = append(videoursl,url)
	//	//fmt.Println("视频地址", url)
	//}

	return videoursl

}

func GetVideoWithTag(tag string) []VideoModel {

	var vides  []VideoModel

	var groupUrl = makeUrls(tag, TagGroup)
	for _,v := range groupUrl {

		body,err := HttpGet(v)
		var Listbody *List
		if err == nil {
			err = json.Unmarshal(body, &Listbody)
			if err != nil {
				fmt.Println(err)
				continue
			}
		}

		if Listbody != nil && len(Listbody.Data) > 0 {
			for _, v := range Listbody.Data {
				v.Tag = tag
				vides = append(vides, v)
			}
			//vides = append(vides, Listbody.Data...)
			//for _, data := range Listbody.Data {
			//
			//	videoweb := fmt.Sprintf(videoweb,data.ID)
			//	url := GetVideoUrlFromweb(videoweb)
			//}

		} else {
			return vides
		}

	}

	return vides


}



func makeUrls(tag string,group int)[]string  {

	var result []string

	for i:= 0;i < group;i++ {
		//fmt.Println(makeUrl(tag, int64(i)))
		result = append(result, makeUrl(tag, int64(i)))
	}

	return result

}


func makeUrl(tag string,index int64) string {

	return fmt.Sprintf(tagVides,tag, index)

}

func GetVideoUrlFromweb(weburl string)string  {

	res, err := http.Get(weburl)
	if err != nil {
		return ""
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return ""
	}

	// Load the HTML document
	doc, _ := goquery.NewDocumentFromReader(res.Body)
	// Find the review items

	url := ""
	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		//band := s.Find("").Text()
		//fmt.Printf("Review %d: %s - %s\n", i, band)

		op, _ := s.Attr("property")
		con, _ := s.Attr("content")
		if op == "og:video" {
			url = con
		}
	})

	return url

}



