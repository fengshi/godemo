package util

import (
	"bufio"
	"bytes"
	"fmt"
	"goDemo/Project/downLoader/loader"
	"io/ioutil"
	"mcw/blockChain/common/util/mcw_logs"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"
)

func init() {
	dir := os.ExpandEnv("$HOME") + "/Downloads/vmate"
	if Exist(dir) == false {
		err := os.MkdirAll(dir, 0777)
		fmt.Println(err)
	}
}

func HttpGetOri(url string) (*http.Response, error) {
	client := http.Client{}
	client.Timeout = time.Second * 5
	return client.Get(url)
}

func HttpGet(url string) ([]byte, error) {
	client := http.Client{}
	client.Timeout = time.Second * 5
	if resp, err := client.Get(url); err != nil {
		mcw_logs.Printf("[error]request->HttpGet get request fail, error:%s\n", err.Error())
		return nil, err
	} else {
		defer resp.Body.Close()

		result, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return result,nil
	}
}

func HttpPost()  {
	param := url.Values{
		"apiver":{"4"},
		"appid":{"com.uc.vmate.app.ios"},
		"appver":{"1.13"},
		"clientid":{"ios_b298560e-0c00-4e18-8214-f3e52afa6ab8"},
		"country":{"CN"},
		"DS":{"P5DcIFQzFnw4zGPLWGn5uBn8CEEUmNFZUErYewo/mYw="},
		"PAGE":{"1"},
		"uuid":{"7323a68e-ac2c-4f80-9623-ae5393c80875"},
		"sign":{"8145813a68142d48f133f61521f1762f"},
	}

	URL := "http://vapi.apk.vmate.in/v2/ugc/discover"
	buffer := bytes.NewBufferString(param.Encode())
	client := http.Client{}
	resp, err := client.Post(URL,"application/x-www-form-urlencoded",buffer)

	fmt.Println(err)
	defer resp.Body.Close()

	if resp.StatusCode == 200 {

		resBody, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Println(string(resBody))
			fmt.Println(resp.Header)
		} else {
			fmt.Println("json 解析失败",err)
		}
	}
}

const bufferSize  = 1024
// Download 下载功能, 第二个可选参数为一个文件夹名称
func Download(url string, Directory string) error {

	if Exist(Directory) == false {
		_ = os.MkdirAll(Directory, os.ModePerm)
	}
	filename := loader.GetFilename()
	filename += ".mp4"
	filePath := filepath.Join(Directory, filename)

	if Exist(filePath) {
		return fmt.Errorf(fmt.Sprintln("文件已存在", url))
	}
	res,  err := http.Get(url)
	if err != nil{
		return err
	}
	defer res.Body.Close()

	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	defer file.Close()
	if err != nil {
		fmt.Println("open log file err", err)
		return err
	}
	w := bufio.NewWriterSize(file, bufferSize)
	defer w.Flush()

	result, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	_ , err = w.Write(result)
	return err
}

