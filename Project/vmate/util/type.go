package util

type List struct {
	Status int          `json:"status"`
	Next   int          `json:"next"`
	Abtag  string       `json:"abtag"`
	RecoId string       `json:"reco_id"`
	Data   []VideoModel `json:"data"`
}

type VideoModel struct {
	Type     string `json:"type"`
	ID       string `json:"id"`
	LikeNum  int64  `json:"like_num"`
	ShareNum int64  `json:"share_num"`
	Tag      string  `json:"tag"`
}

type TagList struct {
	Status int         `json:"status"`
	Abtag  string      `json:"abtag"`
	RecoId string      `json:"reco_id"`
	Next   int         `json:"next"`
	Data   []TagDetail `json:"data"`
}

type TagDetail struct {
	Id          string `json:"id"`
	Hashtag     string `json:"hashtag"`
	VideosTotal int64  `json:"videos_total"`
}

type UrlModel struct {
	Url string `json:"url"`
	Tag string `json:"tag"`
}

func NewUrlModel(url string, tag string) *UrlModel {
	return &UrlModel{Url: url, Tag: tag}
}

type VideoModels []VideoModel
type ByVideoModelsLikeNum struct{ VideoModels }
func (r VideoModels) Len() int      { return len(r) }
func (r VideoModels) Swap(i, j int) { r[i], r[j] = r[j], r[i] }
func (r VideoModels) Less(i, j int) bool { return r[i].LikeNum > r[j].LikeNum }

//{
//"type": "video",
//"poster_raw": "https://imgssl.vmate.in/s/vmt/g/ugc/vd/cover/w5/qp/a5sebw2w5qp.jpg?&gyunoplist=,3,webp;3,260x&a=2",
//"uploader_name": "VMate India",
//"uploader_uid": "5555",
//"like_num": 17388,
//"share_num": 8817,
//"comment_num": 894,
//"poster_height": "1280",
//"poster_width": "720",
//"hashtags": null,
//"title": "",
//"id": "a5sebw2w5qp",
//"uploader_poster": "https://imgssl.vmate.in/s/vmt/g/ugc/user/avatar/55/55/5555.jpg?gyunoplist=,90,WEBP;12;3,100;16,10,10,0",
//"poster": "https://imgssl.vmate.in/s/vmt/g/ugc/vd/cover/w5/qp/a5sebw2w5qp.jpg?gyunoplist=,88,WEBP;12;3,300;16,10,10,0",
//"poster_prefetch": "https://imgssl.vmate.in/s/vmt/g/ugc/vd/cover/w5/qp/a5sebw2w5qp.jpg?gyunoplist=,70,WEBP;12;3,414;16,10,10,0",
//"share_url": "//share008.vmate.com/hashtag_video_detail?app=officialsite&id=a5sebw2w5qp&open=0&src=flow",
//"followed_num": 0,
//"brief": "",
//"create_time": "1552749922",
//"subscript": "Prize"
//},
