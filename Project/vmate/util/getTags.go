package util

import (
	"encoding/json"
	"fmt"
	"goDemo/Project/vmate/config"
	"os"
)

var tagListUrl = "https://www.vmate.com/api/discover?page=%v&pagesize=10&ref="

var loadTags []string

func GetTags(config config.VamteCon) []string {

	if len(config.Vmate.LocalTags) > 0 {
		fmt.Println("使用配置文件中的tags")
		return config.Vmate.LocalTags
	}
	fmt.Printf("拉取网络中的tag...")
	loadTags = make([]string, 0)
	getPageTags(config.Vmate.TagLimit, 1, nil)


	file,err:=os.OpenFile(os.ExpandEnv("$HOME") + "/Desktop/vmate/tag.txt",os.O_CREATE|os.O_WRONLY,os.ModePerm)
	if err !=nil{
		fmt.Println("打开文件有误：",err.Error())
	}
	buffer, _ := json.Marshal(loadTags)
	_,err = file.Write(buffer)
	fmt.Println(err)
	return loadTags
}

func getPageTags(tagLimit int64, page int, taglist *TagList) {
	if page == 0 {
		return
	}

	tagList := getTagListWithPage(page)

	if tagList != nil && tagList.Next > 0 && len(tagList.Data) > 0 {

		for _, data := range tagList.Data {
			if data.VideosTotal >= tagLimit {
				loadTags = append(loadTags, data.Id)
			}

		}
		getPageTags(tagLimit, tagList.Next, taglist)

	} else {
		getPageTags(tagLimit, 0, taglist)
	}

}

func getTagListWithPage(page int) *TagList {
	url := fmt.Sprintf(tagListUrl, page)
	body, err := HttpGet(url)
	var List TagList
	if err == nil {
		err = json.Unmarshal(body, &List)
		if err != nil {
			fmt.Println(err)
		}
	}

	return &List

}
