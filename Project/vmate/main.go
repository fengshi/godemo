package main

import (
	"fmt"
	"goDemo/Project/downLoader/loader"
	"goDemo/Project/vmate/util"
	"os"
)

func main()  {

	fmt.Println("-------------------使用方法---------------------")
	fmt.Println("把vmate 文件夹放在桌面，然后双击vmate文件夹内main打开")
	fmt.Println("视频下载目录：下载 -> vmate")
	fmt.Println("-------------------使用方法---------------------")
	fmt.Println("拉取数据分析中...")
	urls := util.MakeVedioUrsl()
	loader.TotalCount = len(urls)
	fmt.Printf("一共有链接  %d 个 \n", loader.TotalCount)
	loader.LoadCount = 0

	dir := os.ExpandEnv("$HOME") + "/Downloads/vmate"

	fmt.Println(urls[0])
	for i := 0;i < loader.TotalCount;i++ {

		err := util.Download(urls[i].Url,dir + "/" + urls[i].Tag)
		if err == nil {
			loader.LoadCount ++
		}
		if loader.LoadCount % 20 == 0 {
			fmt.Printf("已经下载了  %d 个，还有 %d 个 \n", loader.LoadCount,loader.TotalCount-loader.LoadCount)
		}
	}

}
