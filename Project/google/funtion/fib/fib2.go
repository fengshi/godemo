package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// 使用闭包实现菲波那切数列
func fibo() intgen  {
	a , b := 0,1
	return func() int {
		a, b = b, a + b
		return a
	}
}

type intgen func()int

func (g intgen)Read(p []byte)(n int,err error)  {
	next := g()
	if next > 10000 {
		 return 0, io.EOF
	}
	s := fmt.Sprintf("%d\n", next)
	return strings.NewReader(s).Read(p)

}

func PrintFileContents(read io.Reader)  {
	scaner := bufio.NewScanner(read)
	for scaner.Scan() {
		fmt.Println(scaner.Text())
	}
}

func main()  {
	f := fibo()
	PrintFileContents(f)
}
