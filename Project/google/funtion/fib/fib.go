package fib

import "fmt"

// 使用闭包实现菲波那切数列
func fibonaci() func()int  {
	a , b := 0,1
	return func() int {
		a, b = b, a + b
		return a
	}
}

func main()  {
	f := fibonaci()
	for i := 0; i < 10 ; i++ {
		fmt.Println(f())
	}
}
