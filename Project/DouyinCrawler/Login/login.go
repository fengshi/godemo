package Login

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"myTool/file"
	"net/http"
	"os"
)

var loginurl = "https://lf-hl.snssdk.com/passport/mobile/login/?version_code=6.6.0&pass-region=1&pass-route=1&js_sdk_version=1.16.3.0&app_name=aweme&vid=0CDB2B8A-8499-41A4-9F1F-B403559F1DB3&app_version=6.6.0&device_id=33462919227&channel=App%20Store&mcc_mnc=46001&aid=1128&screen_width=640&openudid=9e82dafa31ca55809bc655102cae52ecd1c75cb3&os_api=18&ac=WIFI&os_version=12.3.1&device_platform=iphone&build_number=66018&device_type=iPhone8,4&iid=75086483700&idfa=436C228C-2B0B-42EE-A40B-B8B87CAE1EF7&password=746f6b343c3c3735363431&mix_mode=1&mobile=2e3d3334323335343534323c3d34"

func Login() ([]*http.Cookie,*LoginRs)  {

	client := http.Client{}

	res ,err := client.Get(loginurl)

	if err != nil {
		return nil, nil
	}


	//保存ck 到本地文件
	cookiePath := "ck.txt"
	if file.PathExist(cookiePath) == true {
		_=os.Remove(cookiePath)
	}

	cok := res.Cookies()
	ckMap := make(map[string]string)
	for _,c := range cok {
		fmt.Println(c.Name,c.Value)
		ckMap[c.Name]=c.Value
	}
	writeToFile(cookiePath, ckMap)

	// 返回登入信息
	buffer,_ := ioutil.ReadAll(res.Body)
	var Login LoginRs
	json.Unmarshal(buffer, &Login)

	fmt.Println(Login)
	return cok,&Login

}

func GetCookies()([]*http.Cookie, error) {


	buffer, err := ioutil.ReadFile("ck.txt")
	if err != nil {
		return nil,err
	}
	var ckMap map[string]string

	err = json.Unmarshal(buffer, &ckMap)
	if err != nil {
		return nil,err
	}

	var result []*http.Cookie

	for k, v := range ckMap {
		c := &http.Cookie{Name:k, Value:v}
		result = append(result, c)
	}
	fmt.Println(result)
	return result, nil
}

func GetHeader()*http.Header  {
	return nil
}


func writeToFile(filePath string,msg interface{}) {

	buffer, _ :=json.Marshal(msg)

	file, err := os.OpenFile(filePath, os.O_CREATE|os.O_RDWR|os.O_APPEND, os.ModePerm)
	defer file.Close()
	if err != nil {
		fmt.Println("open log file err", err)
		return
	}

	file.Write(buffer)

}