package Login


type LoginRs struct {
	Data struct {
		UserID               int64         `json:"user_id"`
		UserIDStr            string        `json:"user_id_str"`
		Name                 string        `json:"name"`
		ScreenName           string        `json:"screen_name"`
		AvatarURL            string        `json:"avatar_url"`
		UserVerified         bool          `json:"user_verified"`
		VerifiedContent      string        `json:"verified_content"`
		VerifiedAgency       string        `json:"verified_agency"`
		IsBlocked            int           `json:"is_blocked"`
		IsBlocking           int           `json:"is_blocking"`
		BgImgURL             string        `json:"bg_img_url"`
		Gender               int           `json:"gender"`
		MediaID              int           `json:"media_id"`
		UserAuthInfo         string        `json:"user_auth_info"`
		Industry             string        `json:"industry"`
		Area                 string        `json:"area"`
		CanBeFoundByPhone    int           `json:"can_be_found_by_phone"`
		Mobile               string        `json:"mobile"`
		Birthday             string        `json:"birthday"`
		Description          string        `json:"description"`
		NewUser              int           `json:"new_user"`
		SessionKey           string        `json:"session_key"`
		IsRecommendAllowed   int           `json:"is_recommend_allowed"`
		RecommendHintMessage string        `json:"recommend_hint_message"`
		Connects             []interface{} `json:"connects"`
		FollowingsCount      int           `json:"followings_count"`
		FollowersCount       int           `json:"followers_count"`
		VisitCountRecent     int           `json:"visit_count_recent"`
		SkipEditProfile      int           `json:"skip_edit_profile"`
		IsManualSetUserInfo  bool          `json:"is_manual_set_user_info"`
	} `json:"data"`
	Message string `json:"message"`
}
