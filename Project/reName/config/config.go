package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
)

type tomlConfig struct {
	Con   Config
}

type Config struct {
	Path string
	Name string
}

var ToCon tomlConfig

func init() {
	var config tomlConfig
	if _, err := toml.DecodeFile("_examples/example.toml", &config); err != nil {
		fmt.Println(err)
		return
	}
}