package main

import (
	"fmt"
	"goDemo/Project/reName/loader"
	"goDemo/Project/reName/util"
	"os"
	"path"
	"path/filepath"
)

func main()  {

	fmt.Println("把视频放到 下载里面的test 文件夹, 双击程序")

	defaultHome  := os.ExpandEnv("$HOME")
	filep := defaultHome + "/Downloads/test"
	files, _  := util.GetAllFiles(filep)
	loader.TotalCount = len(files)

	for _, file := range files {

		filenameWithSuffix := path.Base(file) //获取文件名带后缀
		fileSuffix := path.Ext(filenameWithSuffix) //获取文件后缀
		newpath := filepath.Dir(file) +"/" +loader.GetFilename() +fileSuffix
		loader.LoadCount ++
		os.Rename(file, newpath)
	}

}
