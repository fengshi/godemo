package main



import (
	"fmt"
	"myTool/file"
	"os"
	"path/filepath"
	"strings"
)

func main()  {

	home := os.ExpandEnv("$HOME")
	preferences := fmt.Sprintf("%v/Library/Preferences", home)
	fmt.Println(preferences)

	_, dirs,_ := file.GetAllFilesAndDirs(preferences)

	var goLandDirs []string
	for _, dir := range dirs {
		if strings.HasPrefix(filepath.Base(dir), "PyCharm") {
			goLandDirs = append(goLandDirs, dir)
		}

	}

	for _, dir := range goLandDirs {

		eval := filepath.Join(dir, "eval")
		// 删除eval 下所有文件
		files, err := file.GetAllFiles(eval)
		if err != nil {
			return
		}

		for _, file := range files {
			_ = os.Remove(file)
		}

		// 删除port 和 port.lock
		_ = os.Remove(filepath.Join(dir,"port"))
		_ = os.Remove(filepath.Join(dir,"port.lock"))
	}


}