package main

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

type rq struct {
	Address string `json:"path"`
	Data string	`json:"data"`
}
func main()  {


	url := "http://192.168.31.110:8039/sign"



	body := rq{
		"/pkh/Xy9DXXe7ccshZWFg69VU9cdChZxeT17aX4/",
		"abcdsfsgdf",
	}

	by,err:= json.Marshal(body)
	if err != nil {
		panic(err)
	}
	fmt.Println(body)
	fmt.Println("by",by)

	//Post(url)
	res ,_ := PostJson(url, by)
	fmt.Println(string(res), err)


}


func HttpPost(url string, data []byte) ([]byte, error) {
	client := http.Client{}
	if resp, err := client.Post(url, "application/json;charset=utf-8", bytes.NewReader(data)); err != nil {
		return nil, err
	} else {
		defer resp.Body.Close()
		return ioutil.ReadAll(resp.Body)
	}
}

func Post(urlstr string)  {

	param := url.Values{
		"address":{"/pkh/Xtw8nusRYBJzUqwoFJveg2QXWcDbtHtGJm/"},
		"data":{"abcdsfsgdf"},
	}
	// map -> string
	buffer := bytes.NewBufferString(param.Encode())
	client := http.Client{}
	resp, err := client.Post(urlstr,"application/json;charset=utf-8",buffer)

	fmt.Println(err)
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		//获取json数据
		resBody, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Println(string(resBody))
			fmt.Println(resp.Header)
		} else {
			fmt.Println("json 解析失败",err)
		}
	}

}

func PostJson(url string,param []byte)([]byte, error)  {

	client := http.Client{}

	requestUrl := url
	fmt.Println("=======", url)

	buffer := bytes.NewBuffer(param)

	//read := bytes.NewReader(param)
	fmt.Println(buffer)

	request, err := http.NewRequest("POST", requestUrl, buffer)
	fmt.Println(err)

	//request.Header.Set("Accept-Lanauage", "zh-cn")
	//application/json; charset=utf-8
	request.Header.Set("content-type", "application/json;charset=UTF-8")
	response, err := client.Do(request)

	fmt.Println(err)
	//defer response.Body.Close()


	//fmt.Printf("%v\n%v\n", response.StatusCode, response.Header)
	fmt.Println(response)
	if response.StatusCode > 0 {
		fmt.Println(url)
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(string(data))
	} else {
		fmt.Println("网络请求失败", response.Status)
	}

	by, err := ioutil.ReadAll(response.Body)
	fmt.Println("----!!!", string(by), err)
	return ioutil.ReadAll(response.Body)
}

func HttpGet(url string) ([]byte, error) {

	fmt.Println("url",url)
	client := http.Client{}
	if resp, err := client.Get(url); err != nil {

		return nil, err
	} else {
		defer resp.Body.Close()
		return ioutil.ReadAll(resp.Body)
	}
}