package config

import (
	"fmt"
	"github.com/astaxie/beego/config"
	"github.com/pkg/errors"
)

var (
	AppConfig *Config
)

type Config struct {
	Log_level string
	Log_path string

	CollectConfs []CollectConf
}

type CollectConf struct {
	log_path string
	topic string
}

func loadCollectConf(conf config.Configer) error  {

	var cc CollectConf
	cc.log_path = conf.String("collect::log_path")
	if len(cc.log_path) == 0 {
		return errors.New("invalid collect::log_path")
	}

	cc.topic = conf.String("collect::topic")
	if len(cc.topic) == 0 {
		return errors.New("invalid collect::topic")
	}

	AppConfig.CollectConfs = append(appConfig.CollectConfs,cc)
	return nil


}

func LoadConf(confType string,filePath string) error {

	conf, err := config.NewConfig(confType, filePath)
	if err != nil {
		fmt.Println("new config failed, err:", err)
		return err
	}

	appConfig = &Config{}
	appConfig.Log_level = conf.String("logs::log_level")
	if len(appConfig.Log_level) == 0 {
		appConfig.Log_level = "debug"
	}

	appConfig.Log_path = conf.String("logs::log_path")
	if len(appConfig.Log_path) == 0 {
		appConfig.Log_path = "./logs"
	}

	err = loadCollectConf(conf)
	if err != nil {
		fmt.Println("load config err,err:",err)
		return err
	}
	return nil



}
