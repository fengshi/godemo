package log

import (
	"github.com/astaxie/beego/logs"
	"fmt"
	"goDemo/goIT/logagent/config"
	"encoding/json"
)

func InitLogger() error  {
	con := make(map[string]interface{})

	con["filename"] = config.AppConfig
	//config["level"] = logs.LevelDebug //dug 可以记录所有类别的日志
	con["level"] = logs.LevelInfo //info只能记录 warn 类型
	configStr, err := json.Marshal(con)
	if err != nil {
		fmt.Println("marshal failed, err:", err)
		return err
	}


	logs.SetLogger(logs.AdapterFile, string(configStr))
	return nil
}
