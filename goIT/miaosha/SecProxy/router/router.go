package router

import (
	"github.com/astaxie/beego"
	"goDemo/goIT/miaosha/SecProxy/controller"
)

func init()  {
	//*表示get post 都支持
	beego.Router("/seckill", &controller.SkillController{},"*:Seckill")
	beego.Router("/secinfo", &controller.SkillController{},"get:Secinfo")
}
