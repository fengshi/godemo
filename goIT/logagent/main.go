package main

import (
	"fmt"
	"goDemo/goIT/logagent/config"
	"goDemo/goIT/logagent/log"
)

func main()  {

	filename := "config/log.conf"
	err := config.LoadConf("ini",filename)
	if err != nil {
		fmt.Println("load conf faild,err :", err)
		panic("load conf err")
		return
	}
	err = log.InitLogger()
	if err != nil {
		fmt.Println("load log faild,err :", err)
		panic("load log err")
		return
	}
}
