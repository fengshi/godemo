package log

import (
	"github.com/astaxie/beego/logs"
	"goDemo/goIT/logagent/config"
	"fmt"
	"github.com/gin-gonic/gin/json"
)

func InitLogger() error {
	con := make(map[string]interface{})

	fmt.Println("---",config.AppConfig)
	fmt.Println("---+++", config.AppConfig.Log_level)
	con["filename"] = config.AppConfig
	//config["level"] = logs.LevelDebug //dug 可以记录所有类别的日志
	con["level"] = logs.LevelInfo //info只能记录 warn 类型

	configStr, err := json.Marshal(con)
	if err != nil {
		fmt.Println("marshal failed, err:", err)
		return err
	}

	fmt.Println(configStr)
	//logs.SetLogger(logs.AdapterFile, hex.EncodeToString(configStr))
	return nil
}
