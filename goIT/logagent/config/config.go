package config

import (
	"fmt"
	"github.com/astaxie/beego/config"
	"github.com/pkg/errors"
)

var (
	AppConfig *Config
)

type Config struct {
	Log_level string
	Log_path string

	CollectConfs []CollectConf
}

type CollectConf struct {
	log_path string
	topic string
}

func loadCollectConf(conf config.Configer) error  {

	var cc CollectConf
	cc.log_path = conf.String("collect::log_path")
	if len(cc.log_path) == 0 {
		return errors.New("invalid collect::log_path")
	}

	cc.topic = conf.String("collect::topic")
	if len(cc.topic) == 0 {
		return errors.New("invalid collect::topic")
	}

	AppConfig.CollectConfs = append(AppConfig.CollectConfs,cc)
	return nil


}

func LoadConf(confType string,filePath string) error {

	conf, err := config.NewConfig(confType, filePath)
	if err != nil {
		fmt.Println("new config failed, err:", err)
		return err
	}

	AppConfig = &Config{}
	AppConfig.Log_level = conf.String("logs::log_level")
	fmt.Println(AppConfig.Log_level)
	if len(AppConfig.Log_level) == 0 {
		AppConfig.Log_level = "debug"
	}



	AppConfig.Log_path = conf.String("logs::log_path")
	if len(AppConfig.Log_path) == 0 {
		AppConfig.Log_path = "./logs"
	}
	fmt.Println(AppConfig)
	err = loadCollectConf(conf)
	if err != nil {
		fmt.Println("load config err,err:",err)
		return err
	}
	fmt.Println(AppConfig.CollectConfs)
	return nil



}
