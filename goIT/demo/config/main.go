package main

import (
	"github.com/astaxie/beego/config"
	"fmt"
)

/*
	•	String(key string) string 
	•	Int(key string) (int, error) 
	•	Int64(key string) (int64, error) 
	•	Bool(key string) (bool, error) 
	•	Float(key string) (float64, error)
*/


func main() {
	conf, err := config.NewConfig("ini", "config/logcollect.conf")
	if err != nil {
		fmt.Println("new config failed, err:", err)
		return
	}

	port, err := conf.Int("server::port")
	if err != nil {
		fmt.Println("read server:port failed, err:", err)
		return
	}

	fmt.Println("Port:", port)
	log_level := conf.String("logs::log_level")
	if log_level == "" {
		fmt.Println("read log_level failed, ", err)
		return
	}
	fmt.Println("log_level:", log_level)

	log_path := conf.String("logs::log_path")
	fmt.Println("log_path:", log_path)
}

