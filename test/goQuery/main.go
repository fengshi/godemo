package main

import (
	"bufio"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
	"io"
	"log"
	"myTool/file"
	"myTool/imageSplice"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {


	path := os.Args[1]
	if file.PathExist(path) == false {
		panic(fmt.Sprintf("路径错误 %v", path))
	}
	fmt.Println("当前处理的图片目录：", path)
	// 物品价格
	//path := "/Users/qianjianeng/Desktop/AT100"

	files, _ := file.GetAllFiles(path)

	newDir := path + "/price"
	_ = os.Mkdir(newDir, 0777)
	for _, file := range files {

		fileName := filepath.Base(file)
		// 确保文件是图片
		if strings.HasSuffix(fileName,"g") == false {
			continue
		}

		ID := strings.TrimSuffix(fileName, "N.png")
		ID = ID[4:]
		IDInt,_ := strconv.Atoi(ID)

		fmt.Println(IDInt)


		url := fmt.Sprintf("https://www.clubfactory.com/product/%v", IDInt)
		res, err := HTTPGET(url)

		if err != nil {
			return

		}

		//buf, err := ioutil.ReadAll(res)
		//
		//f, _ := os.Create("1.html")
		//
		//f.Write(buf)

		doc, err := goquery.NewDocumentFromReader(res)
		if err != nil {
			fmt.Println(" qoquery 解析错误", err)
		}
		//span[class=price-num]
		doc.Find(".price-num").Each(func(i int, selection *goquery.Selection) {


			price := selection.Text()
			fmt.Println("price", price)

			//文件copy
			_ ,_= copy(file, newDir+"/" + price + ".png")
		})


	}

	// 格式转换
	newPath := imageSplice.ToJpg(newDir)

	fmt.Println("处理出的文件目录：", newPath)



}

func HTTPGET(url string) (io.Reader, error)  {
	client := http.Client{}
	//2.创建一个请求
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	cookName := &http.Cookie{Name:"gender", Value:"U"}
	cook1 := &http.Cookie{Name:"guest_id", Value:"98d75e466a6af0e88edd4893397c9a34"}
	cook2 := &http.Cookie{Name:"_ga", Value:"GA1.2.1594389991.1560325744"}
	cook3 := &http.Cookie{Name:"_gid", Value:"GA1.2.1929814746.1564629253"}
	cook4 := &http.Cookie{Name:"language_code", Value:"es-MX"}
	cook5 := &http.Cookie{Name:"country_code", Value:"in"}
	cook6 := &http.Cookie{Name:"symbol", Value:"%u20B9"}

	//添加cookie
	request.AddCookie(cookName)
	request.AddCookie(cook1)
	request.AddCookie(cook2)
	request.AddCookie(cook3)
	request.AddCookie(cook4)
	request.AddCookie(cook5)
	request.AddCookie(cook6)


	//request.Header.Set("if-none-match", "17d4e-jbEQN3WAzGFSSRMUAU2YH4P1444")
	//request.Header.Set("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36")
	//request.Header.Set("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3")
	//request.Header.Set("accept-encoding", "gzip, deflate, br")
	//request.Header.Set("accept-language", "zh-CN,zh;q=0.9,en;q=0.8")
	response, err := client.Do(request)

	if err != nil {
		return nil, err
	}

	//return response.Body, nil
	//defer response.Body.Close()
	bodyReader := bufio.NewReader(response.Body)
	e := determinEncodeing(bodyReader)
	// 转换为 utf8
	utf8Reader := transform.NewReader(bodyReader, e.NewDecoder())


	return utf8Reader, nil

}

func determinEncodeing (r *bufio.Reader) encoding.Encoding  {

	bytes, err := r.Peek(1024)
	if err != nil {
		log.Println("deternime code error")
		return unicode.UTF8
	}
	e,_, _ := charset.DetermineEncoding(bytes,"")
	return e
}


func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}
	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("Xs is not a regular file”, src")
	}
	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()
	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}