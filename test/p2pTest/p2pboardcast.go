package main

import (
	"fmt"
	"log"
	"net"
	"sync"
	"time"
)

func main() {
	//1.生成udp协议的地址
	udpAddr, err := net.ResolveUDPAddr("udp", ":6666")
	checkErr(err)
	//2.绑定和监听端口
	udpconn, err := net.ListenUDP("udp", udpAddr)
	checkErr(err)

	go Receive(udpconn)

	for {
		time.Sleep(2 * time.Second)

		udpAddr, _ := net.ResolveUDPAddr("udp", "255.255.255.255:6666")
		//2.连接udp协议的服务端
		sendConn, _ := net.DialUDP("udp", nil, udpAddr)
		//3.发送数据
		sendConn.Write([]byte("send: hello peer  I am 11" + "\t"))
	}

}

func Receive(conn *net.UDPConn) {
	for {
		data := make([]byte, 1024)

		n, addr, err := conn.ReadFromUDP(data)
		if err != nil {
			fmt.Println(err)
		}

		if addr.IP.String() == GetLocalIp() {
			continue
		}

		fmt.Println(GetLocalIp())
		fmt.Println(string(data[:n]), addr)
	}

}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

var once sync.Once

func GetLocalIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err.Error())
	}
	var IP string
	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				IP = ipnet.IP.String()

				once.Do(func() {
					fmt.Println("本机IP: ", IP)
				})

				break
			}
		}
	}
	return IP
}
