// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"errors"
)

//var cfgFile string
var nodename string
var ip int

// clientCmd represents the client command

//mycli client --nodename "asdfa"

var clientCmd = &cobra.Command{
	Use:   "client",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("client called")

		nodeName ,_ :=cmd.Flags().GetString("nodename")
		if len(nodeName) > 0 {
			fmt.Println("nodeName",nodeName)
		}


		nodeLists ,_ :=cmd.Flags().GetStringArray("nodelists")
		if len(nodeLists) > 0 {
			fmt.Println("nodeList",nodeLists)
		}

		nodeNumber ,_ := cmd.Flags().GetStringToInt("number")
		if len(nodeNumber) > 0 {
			fmt.Println("nodeNumber:",nodeNumber)
		}

	},

}


var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Echo anything to the screen",
	Long: `echo is for echoing anything back.
Echo works a lot like print, except it has a child command.`,
	//Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("Print: " + strings.Join(args, "-"))
		fmt.Println("1.0.0")
	},
}

var cmd = &cobra.Command{
	Short: "hello",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("requires at least one arg")
		}

		return fmt.Errorf("invalid color specified: %s", args[0])
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello, World!")
	},
}



func init() {
	rootCmd.AddCommand(clientCmd)
	rootCmd.AddCommand(versionCmd)

	//节点名称
	//调用
	//go run main.go client --nodename abc
	//go run main.go client -n abc
	clientCmd.Flags().StringP("nodename","n","","des string")
	//节点列表
	//go run main.go client -l "a, b, c"
	clientCmd.Flags().StringArrayP("nodelists","l",nil,"des ass")

	//节点number
	//go run main.go client -b a=12
	clientCmd.Flags().StringToIntP("number","b",nil,"des")


	//clientCmd.Flags().String("aa","a","")
	//clientCmd.Flags().StringP("bb","b","","des")
	//clientCmd.Flags().StringVarP(&nodename,"cc","c","","des")




	//versionCmd.Flags().String("version","","")
	//clientCmd.Flags().String("name2","n2","node2")
	//clientCmd.Flags().StringVarP(&nodename, "name", "n", "", "node")
	//rootCmd.Flags().IntVarP(&ip, "age", "i", 0, "node age")

	//clientCmd.Flags().StringSliceP("arr","r", nil, "test arr")
	//
	//clientCmd.Flags().String("f", "", "test")
	//clientCmd.Flags().StringP("aaa", "a", "", "test")
	//
	//clientCmd.PersistentFlags().String("foo", "", "A help for foo")
	//clientCmd.Flags().String("fools", "", "A help for foos")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// clientCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// clientCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
