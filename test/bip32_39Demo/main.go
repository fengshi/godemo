package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/hex"
	"fmt"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/tyler-smith/go-bip32"
	"github.com/tyler-smith/go-bip39"
	"math/big"
)

func main()  {

	//0.mnemonic code Generate
	entropy, err := bip39.NewEntropy( 128)
	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		panic("generate mnemonic code err")
	}
	fmt.Println(mnemonic)
	
	return

	// 熵和助记词可以互相转化
	//recoverEntropy,err := bip39.EntropyFromMnemonic(mnemonic)

	//I.seed
	//seed, err ;= bip32.NewSeed()
	seed := bip39.NewSeed(mnemonic,"")
	if err != nil {
		panic( "generate seed error 11")
	}
	fmt.Println( "seed:", hexString(seed))
	//2,masterKey
	masterKey, err := bip32.NewMasterKey(seed)
	if err != nil {
		panic( "generate master key error")
	}


	//主秘钥
	masterKeyPriHex := hexString(masterKey.Key)
	masterKeyPri := StringToEcdsa(masterKeyPriHex)
	masterKeypub := masterKeyPri.PublicKey

	fmt.Println("master pri", masterKeyPriHex)
	fmt.Println("这里一般会进行压缩，master pub", hexPublicKey(masterKeypub))


	fmt.Println("masterKey.ChainCode", hexString(masterKey.ChainCode))
	fmt.Println(hexString(masterKey.ChildNumber))
	fmt.Println(hexString(masterKey.Key))
	fmt.Println(masterKey.String())
	se, _ := masterKey.Serialize()
	fmt.Println(hexString(se))

	fmt.Println(  "masterKey:", masterKey)
	//3.wallets/account
	account0, err := masterKey.NewChildKey(0)
	if err != nil {
		panic( "generate account err")
	}
	fmt.Println( "accountO Key:", account0)
	//4,chains
	chain0, err := account0.NewChildKey(0)
	if err != nil {
		panic( "generate chain err")
	}
	fmt.Println("chainO Key:11", chain0)

	//5.Address
	address0, err := chain0.NewChildKey(0)
	if err != nil {
		panic("generate address err")
	}
	fmt.Println( "addressO Key:11", hexString(address0.Key))
	fmt.Println( "length:11", len(address0.Key))
	//6.generate Address
	//eth address
	// public Key
	address0PubKey := address0.PublicKey()
	fmt.Println("addressO public key:11", address0PubKey)
	//public key hash
	publicKeyHash := crypto.Keccak256(address0PubKey.Key[1:])
	fmt.Println(len(publicKeyHash))
	address := hexString(publicKeyHash[12:])
	fmt.Println("Ox11" + address)
	fmt.Println(len(address))
}



func hexString(b []byte)string  {
	return hex.EncodeToString(b)
}



// ecdsa私钥 转化为 16 进制私钥
func PrivateToString(priKey ecdsa.PrivateKey) string {

	return hex.EncodeToString(crypto.FromECDSA(&priKey))
}


func StringToEcdsa(prikey string) *ecdsa.PrivateKey  {
	private,err := hex.DecodeString(prikey)

	fmt.Println(err)

	ecdsaPri := PrivKeyFromBytes(private)

	return ecdsaPri
}



// 字符串私钥转 ecdsa 私钥
// byte ==> ecdsa
func PrivKeyFromBytes(pk []byte) (*ecdsa.PrivateKey) {

	curve := elliptic.P256()
	x, y := curve.ScalarBaseMult(pk)

	priv := &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: curve,
			X:     x,
			Y:     y,
		},
		D: new(big.Int).SetBytes(pk),
	}

	return priv
}

func hexPublicKey(pubkey ecdsa.PublicKey) string  {
	publicKey := append(pubkey.X.Bytes(), pubkey.Y.Bytes()...)
	return hexString(publicKey)
}