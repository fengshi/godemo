package main

import (
	"fmt"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
	"github.com/tyler-smith/go-bip39"
)
//创建钱包
func main()  {


	for i:=0;i<10;i++ {
		CreateWallet()
	}

}

func CreateWallet()  {
	entropy, err := bip39.NewEntropy( 128)
	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		panic("generate mnemonic code err")
	}
	fmt.Println(mnemonic)
	wallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		return
	}
	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, err := wallet.Derive(path, false)
	if err != nil {
		return
	}

	address, err := wallet.AddressHex(account)
	if err != nil {
		return
	}
	fmt.Println(address)
}