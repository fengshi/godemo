package Tool

import "strings"

func GetWebName(url string) string  {
	slis := strings.Split(url, "www.")
	return strings.Split(slis[1], ".")[0]
}

func CheckUrl(url string) string  {
	if strings.HasPrefix(url,"http") == false {
		return "https://"+url
	} else {
		return url
	}
}