package main

import (
	"gopkg.in/mgo.v2/bson"
	"fmt"
	"math/big"
	"math"
)



type Person struct {
	ID    bson.ObjectId `_id`
	Name string
	Age int
}

const  (
	dbUrl = "127.0.0.1:27017"
)

func main(){

	//session,err := mgo.Dial(dbUrl)
	//
	//if err != err {
	//	println(err)
	//}
	//defer session.Close()
	//session.SetMode(mgo.Monotonic, true) //设置一致性模式
	//
	////选择数据库
	//db := session.DB("mydb")
	//
	////选择数据表
	////没有会自动创建
	//collection := db.C("user2")

	a := big.NewInt(1234567891234567)

	b := big.NewInt(23456789344)

	c := big.NewInt(1)
	c.Mul(a, b)

	fmt.Println(c)
	fmt.Println(c.Int64())

	//d := 9223372036854775807 + 1
	fmt.Println("max",math.MaxInt64)
	fmt.Println(math.Pow(2, 64) - 1)
	fmt.Printf("%v", math.Pow(2, 16) - 1)
	////增  单条插入
	//p1 := Person{bson.NewObjectId(),"lisi2", 12}
	//p2 := Person{bson.NewObjectId(),"lisi2", 12}
	//
	//err = collection.Insert(&p1, &p2)
	//fmt.Println(err)
	/*
	//批量插入
	var arr []interface{}
	arr = append(arr, p1)
	arr = append(arr, p2)
	err = collection.Insert(arr...)
	if err != err {
		println(err)
	}



	//查第一条
	result := Person{}
	collection.Find(bson.M{"age":12}).One(&result)
	fmt.Println("restlt",result)

	//查多条
	results := []Person{}
	collection.Find(bson.M{"age":12}).All(&results)
	fmt.Println("results",results, len(results))



	//查表中数据总数
	count, _:= collection.Find(nil).Count()
	fmt.Println(count)

	//查所有
	arr2 := make([]Person, 0)
	iterNew := collection.Find(nil).Iter()
	err = iterNew.All(&arr2)
	if err != nil {
		panic(err)
	}
	fmt.Println("arr2",arr2)

	//改(不加set就是覆盖)
	collection.Update(bson.M{"name":"zhangsan"}, bson.M{"$set":bson.M{"age":22}})
	collection.Update(bson.M{"age":22},bson.M{"$set":bson.M{"age":23}})
	collection.Update(bson.M{"name":"lisi"}, bson.M{"name":"lisi2","age":22})

	//批量更新
	collection.UpdateAll(bson.M{"age":22}, bson.M{"$set":bson.M{"age":44}})




	//删

	//删除符合条件的第一条
	//collection.Remove(bson.M{"name": "lisi2"})

	//删除所有
	_, err = collection.RemoveAll(bson.M{"name": "lisi"})
	if err != err {
		println(err)
	}



	//根据ID删除
	var wangwu = Person{}
	collection.Find(bson.M{"name":"wangwu"}).One(&wangwu)
	fmt.Println(wangwu, wangwu.ID.Hex(), wangwu.ID.String())
	err =collection.RemoveId(wangwu.ID)
	fmt.Println("delete",err)

   */

   //删除集合
   //collection.DropCollection()


}




