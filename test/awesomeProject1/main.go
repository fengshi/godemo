package main

import ("encoding/binary"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
)

var bloomBitsPrefix = []byte("B")

//var HA common.
func main() {

	by := []byte("1234567890123456789012345678901234567890")[:32]
	hash := common.Hash{}
	hash.SetBytes(by)

	fmt.Println(bloomBitsKey(111100000, 222002000, hash))
}


func bloomBitsKey(bit uint, section uint64, hash common.Hash) []byte {
	key := append(append(bloomBitsPrefix, make([]byte, 10)...), hash.Bytes()...)

	fmt.Println(key)
	binary.BigEndian.PutUint16(key[1:], uint16(bit))
	binary.BigEndian.PutUint64(key[3:], section)

	fmt.Println(key)
	return key
}