package main
import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"net/http"
	"time"
)

var (
	//定义外部输入文件名字
	cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file.")
	meprofile = flag.String("meprofile", "", "write memory profile to file.")


)

func main() {

	http.ListenAndServe("8081", nil)
	log.Println("begin")

	flag.Parse()

	if *cpuprofile != "" {

		f, err := os.Create(*cpuprofile)

		if err != nil {

			log.Fatal(err)

		}
		pprof.StartCPUProfile(f)

		defer pprof.StopCPUProfile()


	}

	for i := 0; i < 30; i++ {

		nums := fibonacci(i)

		fmt.Println(nums)

	}
	time.Sleep(100 * time.Second)
}

//递归实现的斐波纳契数列

func fibonacci(num int) int {

	if num < 2 {

		return 1

	}
	return fibonacci(num-1) + fibonacci(num-2)
}

