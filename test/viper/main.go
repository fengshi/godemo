package main

import (
	"fmt"
	"github.com/spf13/viper"
)

func main() {

	//设置
	viper.SetDefault("name", "zhangsan")
	viper.SetDefault("mmp", map[string]string{"t": "tags", "c": "category"})

	//读取
	str := viper.Get("name")
	fmt.Println(str)

	m := viper.Get("mmp")
	fmt.Printf("%v--%T \n", m, m)

	//初始化配置文件
	viper.SetConfigName("sys")  //  设置配置文件名 (不带后缀)
	viper.SetConfigType("json") // 文件后缀
	//viper.AddConfigPath("conf")               // 比如添加当前目录
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // 搜索路径，并读取配置数据
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err)) ////viper.SetConfigType("toml")
	}
	p := viper.GetString("port")

	fmt.Println("port", p)

	p2 := viper.GetString("system.port")

	fmt.Println("port2", p2)

	//viper.SetConfigName("sys") //  设置配置文件名 (不带后缀)
	//
	////viper.AddConfigPath("/etc/appname/")   // 第一个搜索路径
	////viper.AddConfigPath("$HOME/.appname")  // 可以多次调用添加路径
	//viper.AddConfigPath("conf")               // 比如添加当前目录
	//err := viper.ReadInConfig() // 搜索路径，并读取配置数据
	//if err != nil {
	//    panic(fmt.Errorf("Fatal error config file: %s \n", err))
	//}
	//p := viper.GetString("port")
	//fmt.Println(p)
	//d :=viper.GetString("dbName")
	//fmt.Println(d)

}
