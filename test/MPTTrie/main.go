package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/ethereum/go-ethereum/trie"
	"math/big"
	"time"
)


type GenesisState struct {
	Accounts []*GenesisAccount `json:"accounts"`
}

type Coin struct {
	Token string `json:"denom"`
	Amount string `json:"amount"`
	Ticker string	`json:"ticker"`
}
type GenesisAccount struct {
	Address string       `json:"address"`
	Coins   []Coin      `json:"coins"`
}

type Lock struct {
	Start time.Time `json:"start"`

}
func main() {



	//
	//
	//str:= "7b0a20202020226163636f756e7473223a205b0a2020202020207b0a20202020202020202261646472657373223a202263727973746f61636331387a6a61757a736e6b34376130796672687067383738756b326a737675686770783771727339222c0a202020202020202022636f696e73223a205b0a202020202020202020207b0a20202020202020202020202022746f6b656e223a202263727973746f222c0a20202020202020202020202022616d6f756e74223a202231303030303030303030303030303030222c0a202020202020202020202020227469636b6572223a202253544f220a202020202020202020207d0a20202020202020205d0a2020202020207d0a202020205d0a20207d"
	////123 10 32 32 32 32 34 97 99 99 111 117 110 116 115 34 58 32 91 10 32 32 32 32 32 32 123 10 32 32 32 32 32 32 32 32 34 97 100 100 114 101 115 115 34 58 32 34 99 114 121 115 116 111 97 99 99 49 56 122 106 97 117 122 115 110 107 52 55 97 48 121 102 114 104 112 103 56 55 56 117 107 50 106 115 118 117 104 103 112 120 55 113 114 115 57 34 44 10 32 32 32 32 32 32 32 32 34 99 111 105 110 115 34 58 32 91 10 32 32 32 32 32 32 32 32 32 32 123 10 32 32 32 32 32 32 32 32 32 32 32 32 34 116 111 107 101 110 34 58 32 34 99 114 121 115 116 111 34 44 10 32 32 32 32 32 32 32 32 32 32 32 32 34 97 109 111 117 110 116 34 58 32 34 49 48 48 48 48 48 48 48 48 48 48 48 48 48 48 48 34 44 10 32 32 32 32 32 32 32 32 32 32 32 32 34 116 105 99 107 101 114 34 58 32 34 83 84 79 34 10 32 32 32 32 32 32 32 32 32 32 125 10 32 32 32 32 32 32 32 32 93 10 32 32 32 32 32 32 125 10 32 32 32 32 93 10 32 32 125]
	//
	//buffer,_ :=hex.DecodeString(str)
	//
	//fmt.Println(buffer)
	//
	//genesisState := new(GenesisState)
	//err := json.Unmarshal(buffer, genesisState)
	//if err != nil {
	//
	//	panic(err)
	//}
	//
	//fmt.Println(genesisState)
	//
	//fmt.Println(string(buffer))
	//
	//fmt.Println(genesisState.Accounts[0].Coins[0].Amount)
	//
	//
	//return


	// 操作commit 才会将内存中的改动写入diskdb中
	diskdb := ethdb.NewMemDatabase()
	triedb := trie.NewDatabase(diskdb)

	diskdb,_ := ethdb.NewLDBDatabase("./db", 1024, 256)

	triedb := trie.NewDatabase(diskdb)

	// 第一棵树
	t1, _ := trie.New(common.Hash{}, triedb)
	//设置缓存大小
	t1.SetCacheLimit(100)
	//实际使用时，Update中的 value是需要先经过rlp编码, 这里测试暂不编码
	t1.Update([]byte("120"), []byte("qwerqwerqwerqwerqwerqwerqwerqwer"))
	root, _ := t1.Commit(nil)  //trie.Commit需要了解
	triedb.Commit(root, true)  //这个就是将trie提交到db了



	//根据key查找某个trie是否存在

	t2,_ := trie.New(root, triedb)
	blob, err := t2.TryGet([]byte("120"))
	fmt.Println(blob, HexToString(t2.Hash().Bytes()),err)


	fmt.Println(string(blob))

	//解码
	//rlp.DecodeBytes(blob, &bts)
	//fmt.Println("bts====", bts)

	// 添加node
	t3, _ := trie.New(root, triedb)
	t3.Update([]byte("121"), []byte("asdfasdfasdfasdfasdfasdfasdfasdf"))
	root, _ = t3.Commit(nil)
	triedb.Commit(root, true)

	//删除node
    t4,_ := trie.New(root, triedb)
    t4.Delete([]byte("121"))
	root, _ = t4.Commit(nil)
	triedb.Commit(root, true)

    //检查 121 是否删除成功
	t5,_ := trie.New(root, triedb)
	var bts []byte
	bts, err = t5.TryGet([]byte("121"))
	fmt.Println("t5",bts, HexToString(t5.Hash().Bytes()),err)

	IteratorTrie(t5)


	// 获取证明

	proof := ethdb.NewMemDatabase()
	err = t1.Prove([]byte("120"), 0, proof)

	if err != nil {
		panic(err)
	}

	value,node,err := trie.VerifyProof(root,[]byte("120"),proof)

	if err == nil {
		fmt.Println("mpt 证明通过", value, node)
	}

	//第二课树
	tt1,_ := trie.New(common.Hash{}, triedb)
	tt1.Update([]byte("200"), []byte("qwerqwagaderqwer1"))
	tt1.Update([]byte("201"), []byte("qwerqwadferqwer2"))
	tt1.Update([]byte("202"), []byte("qweadfrqweasrqwer3"))
	tt1.Update([]byte("203"), []byte("qweavdfgrqagwaerqwer4"))
	root1 , _ := tt1.Commit(nil)
	triedb.Commit(root1, true)
	fmt.Println("tt1", HexToString(tt1.Hash().Bytes()),err)


	k,_ := rlp.EncodeToBytes([]byte("200"))
	HexToString(k)

	fmt.Println(k)
	//for _,key := range diskdb.Keys() {
	//	fmt.Println("//",HexToString(hexToCompact(key)))
	//	fmt.Println(HexToString(key))
	//
	//}



	//
	//re, err := diskdb.Get(k)
	//fmt.Println("++++++++++++", string(re), err)
	//
	//
	//arr := []byte("adadfffadfd1")
	//fmt.Println(hashWithSlice(arr))
	//
	//IteratorTrie(tt1)
	//ttt("")

}

func keybytesToHex(str []byte) []byte {
	l := len(str)*2 + 1
	var nibbles = make([]byte, l)
	for i, b := range str {
		nibbles[i*2] = b / 16
		nibbles[i*2+1] = b % 16
	}
	nibbles[l-1] = 16
	return nibbles
}

func hexToKeybytes(hex []byte) []byte {
	if hasTerm(hex) {
		hex = hex[:len(hex)-1]
	}
	if len(hex)&1 != 0 {
		panic("can't convert hex key of odd length")
	}
	key := make([]byte, len(hex)/2)
	decodeNibbles(hex, key)
	return key
}

func HexToString(byte []byte)string  {
	return hex.EncodeToString(byte)
}

func hexToCompact(hex []byte) []byte {
	terminator := byte(0)
	if hasTerm(hex) {
		terminator = 1
		hex = hex[:len(hex)-1]
	}
	buf := make([]byte, len(hex)/2+1)
	buf[0] = terminator << 5 // the flag byte
	if len(hex)&1 == 1 {
		buf[0] |= 1 << 4 // odd flag
		buf[0] |= hex[0] // first nibble is contained in the first byte
		hex = hex[1:]
	}
	decodeNibbles(hex, buf[1:])
	return buf
}

func hasTerm(s []byte) bool {
	return len(s) > 0 && s[len(s)-1] == 16
}
func decodeNibbles(nibbles []byte, bytes []byte) {
	for bi, ni := 0, 0; ni < len(nibbles); bi, ni = bi+1, ni+2 {
		bytes[bi] = nibbles[ni]<<4 | nibbles[ni+1]
	}
}
// 用trie计算数组的hash
func hashWithSlice(arr []byte) string  {
	keybuf := new(bytes.Buffer)
	trie := new(trie.Trie)
	for i := 0; i < len(arr); i++ {
		keybuf.Reset()
		rlp.Encode(keybuf, uint(i))
		enc, _ := rlp.EncodeToBytes(arr[i])
		trie.Update(keybuf.Bytes(), enc)
	}
	return HexToString(trie.Hash().Bytes())
}


//树的迭代
func IteratorTrie(tree *trie.Trie)  {
	fmt.Println("------start-------")
	it := trie.NewIterator(tree.NodeIterator(nil))

	for it.Next() {
		blob, err := tree.TryGet(it.Key)
		fmt.Println(string(blob),err)
	}
	fmt.Println("------end-------")
}

func ttt(_ string)  {
	fmt.Println("a")
}