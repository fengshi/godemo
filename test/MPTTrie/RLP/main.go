package main

import "fmt"
import "github.com/ethereum/go-ethereum/rlp"

type MyCoolType2 struct {
	Name string
	A, B uint
}

func ExampleEncoder() {
	var t *MyCoolType2 // t is nil pointer to MyCoolType
	// 编码的例子
	bytes_, _ := rlp.EncodeToBytes(t)
	fmt.Printf("%v → %X\n", t, bytes_)

	t = &MyCoolType2{Name: "foobar", A: 10, B: 6}
	bytes_, _ = rlp.EncodeToBytes(t)
	fmt.Printf("%v → %X\n", t, bytes_)

	// 解码的例子
	var val MyCoolType2
	err := rlp.DecodeBytes(bytes_, &val)
	fmt.Printf("MyCoolType2:err=%+v,val=%+v\n", err, val)
}

func main() {
	ExampleEncoder()
}

func base64Encode()  {
	
}
