package main

import (
	"fmt"
	"reflect"
)

type User struct {
	name string
	age int
	sex string
}

func (u User)sayHello(name string)  {
	fmt.Println(name, "I am", u.name)
}

func main() {

	var us = User{"张三", 23,"man"}
	getObjectInfo(us)
}

func getObjectInfo(obj interface{})  {

	t := reflect.TypeOf(obj)
	fmt.Println("TYpe:", t.Name())

	if k := t.Kind(); k != reflect.Struct {
		fmt.Println("obj 不是结构体类型")
		return
	}

	v := reflect.ValueOf(obj)

	fmt.Println(v, "==")
	fmt.Printf("%T",v)
	fmt.Println(t.NumMethod(), "--",t.NumField())


	for i := 0; i < t.NumField() ; i++  {
		//f := t.Field(i)
		//val := v.Field(i).Interface()
		//val := v.FieldByName(f.Name).Interface()
		//val := t.Field(i)
		//
		//fmt.Println(f.Name,f.Type,)
		f := v.Field(i)
		fmt.Printf("%d: %s %s = %v\n", i, t.Field(i).Name, f.Type(), f.Interface())
	}

	//for i := 0; i < t.NumMethod() ; i++  {
	//	m := t.Method(i)
	//	fmt.Println(m.Name,m.Type)
	//}


}


