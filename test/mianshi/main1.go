package main

import (
	"fmt"
)

func main() {

	arr1 := []int{1, 9, 6, 8}
	arr2 := []int{2, 7, 1, 6}

	res := SortArrs(arr1, arr2)

	fmt.Println(res)


}

// 采用map 相同key 覆盖的特性
func SortArrs(arr1, arr2 []int) []int {

	arr1 = append(arr1, arr2...)
	m := make(map[int]bool)
	for _, v := range arr1 {
		m[v] = true
	}
	var res []int
	for k := range m {
		res = append(res, k)
	}
	return res
}
