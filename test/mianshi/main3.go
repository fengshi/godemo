package main

import (
	"fmt"
	"sort"
	"strconv"
)

//卡牌
var CARDS = []string{"2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"}

func main() {

	str1 := "4579K"
	str2 := "AAAA2"
	v := Output(str1, str2)
	fmt.Println(v)

}

func Output(input1, input2 string) int {

	niu1, v1 := cateNiu(changeValue(input1))
	niu2, v2 := cateNiu(changeValue(input2))

	if niu1 == niu2 {

		if v1 > v2 {
			return 1
		} else if v2 > v1 {
			return -1
		} else { //比最大的一张牌谁打
			return maxCardcompare(input1, input2)
		}

	} else if niu1 == true {

		return 1

	} else {
		return -1
	}

}

func getValue(str string) int {
	switch str {
	case "T", "J", "Q", "K":
		return 10
	case "A":
		return 1
	default:
		v, _ := strconv.Atoi(str)
		return v
	}
}

// 将字符串转化为数值
func changeValue(str string) []int {

	var res []int

	n := len(str)
	for i := 0; i < n; i++ {
		v := getValue(string(str[i]))
		res = append(res, v)
	}
	return res

}

//计算牛几
func cateNiu(cards []int) (bool, int) {
	// 计算总点数
	sums := 0
	for i := 0; i < len(cards); i++ {
		if cards[i] > 10 {
			cards[i] = 10
		}
		sums += cards[i]
	}
	//牛牛
	if sums%10 == 0 {
		return true, 0
	}
	// 牛1 ~ 牛9
	bull := 0
	for i := 0; i < len(cards)-2; i++ {
		cardI := cards[i]
		for j := i + 1; j < len(cards); j++ {
			cardJ := cards[j]
			for k := j + 1; k < len(cards); k++ {
				cardK := cards[k]
				total := cardI + cardJ + cardK
				if total%10 == 0 {
					n := (sums - total) % 10
					if bull < n {
						bull = n
					}
				}
			}
		}
	}

	if bull == 0 {
		return false, bull
	} else {
		return true, bull
	}
}

//比较5张牌最大的那张
// 思路
/*
遍历卡牌，找出每张卡牌对应的索引，A 放在的最后，所以A的索引最大，不需要额外处理
将两组索引从大到下排序，遍历比对索引的大小即可，
*/
func maxCardcompare(input1, input2 string) int {

	n := len(input1)

	var arr1 []int
	var arr2 []int
	for i := 0; i < n; i++ {

		str1 := string(input1[i])
		str2 := string(input2[i])

		for index, v := range CARDS {
			if str1 == v {
				arr1 = append(arr1, index)
			}
			if str2 == v {
				arr2 = append(arr2, index)
			}
		}

	}

	sort.Ints(arr1)
	sort.Ints(arr2)

	for i := n; i > 0; i-- {

		if arr1[i] > arr2[i] {
			return 1
		} else if arr1[i] < arr2[i] {
			return -1
		}
	}

	return 0
}
