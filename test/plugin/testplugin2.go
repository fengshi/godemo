package main

import (
	"fmt"
	"plugin"
)

var plug *plugin.Plugin

func init() {

	var err error
	plug, err = plugin.Open("testplugin.so")
	if err != nil {
		panic(err)
	}
}

func SSS()string  {
	f3, err := plug.Lookup("Callback")
	if err != nil {
		panic(err)
	}
	name := f3.(func(string)(string))("bobq22")
	fmt.Println(name)
	return name
}

func Qu(k string)string  {
	f3, err := plug.Lookup("Query")
	if err != nil {
		panic(err)
	}
	name := f3.(func(string)(string))(k)
	fmt.Println(name)
	return name
}