package main

import (
	"fmt"
	"plugin"
)

//编译
//go build -buildmode=plugin testplugin.go
//编译完后我们可以看到当前目录下有一个 testplugin.so 文件
//我们也可以通过类似如下命令来生成不同版本的插件

//go build -o testplugin_v1.so -buildmode=plugin testplugin.go
func main() {


	//加载动态库
	p, err := plugin.Open("testplugin.so")
	if err != nil {
		panic(err)
	}
	//查找函数
	f, err := p.Lookup("Hello")
	if err != nil {
		panic(err)
	}
	//转换类型后调用函数
	f.(func())()


	//带参数的函数 无返回值
	f2, err := p.Lookup("Version")
	if err != nil {
		panic(err)
	}
	f2.(func(string))("new version")


	//带参数有返回值
	f3, err := p.Lookup("Callback")
	if err != nil {
		panic(err)
	}
	name := f3.(func(string)(string))("bob")
	fmt.Println(name)


	//动态库调用动态库 无参数有返回值
	p2, err := plugin.Open("testplugin2.so")
	if err != nil {
		panic(err)
	}

	f4, err := p2.Lookup("SSS")
	if err != nil {
		panic(err)
	}
	name2 := f4.(func()(string))()
	fmt.Println(name2)


	f5, err := p2.Lookup("Qu")
	if err != nil {
		panic(err)
	}
	v := f5.(func(string)(string))("b")
	fmt.Println(v)



}