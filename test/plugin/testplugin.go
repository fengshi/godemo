package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

var Map map[string]string

func init() {
	fmt.Println("world")
	//我们还可以做其他更高阶的事情，比如 platform.RegisterPlugin({"func": Hello}) 之类的，向插件平台自动注册该插件的函数
	file1, _:= os.Open("data1")
	defer file1.Close()

	bufferByte, _ := ioutil.ReadAll(file1)
	json.Unmarshal(bufferByte, &Map)

	if Map == nil {
		Map = make(map[string]string)
	}
	
}

func Hello() {
	fmt.Println("hello")
	Map["a"] = "alice"
	Map["b"] = "bob"
	Map["c"] = "com"

	savedata()
}

func Version(v string)  {
	fmt.Println("sss", v)
}

func Callback(name string) string  {
	return name + "alice"
}

func Query(k string)string  {
	return Map[k]
}

func savedata()  {
	by, _ := json.Marshal(Map)
	file1, _ := os.Create("data1")
	defer file1.Close()
	file1.Write(by)
}