package wallet

import (
	"fmt"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/tyler-smith/go-bip39"
	"github.com/tyler-smith/go-bip32"
)

func Wallet()  {

	//0.mnemonic code Generate
	entropy, err := bip39.NewEntropy( 128)
	fmt.Println("====", len(entropy))
	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		panic("generate mnemonic code err")
	}
	fmt.Println(  "mnemonic code mnemonic")
	//I.seed
	//seed, err ;= bip32.NewSeed()
	seed := bip39.NewSeed(mnemonic,"")
	if err != nil {
		panic( "generate seed error 11")
	}
	fmt.Println( "seed:, byteToHex(seed)")
	//2,masterKey
	masterKey, err := bip32.NewMasterKey(seed)
	if err != nil {
		panic( "generate master key error")
	}
	fmt.Println(  "masterKey:", masterKey)

	fmt.Println("masterKey.ChainCode", masterKey.ChainCode)

	//3.wallets/account
	account0, err := masterKey.NewChildKey(0)
	if err != nil {
		panic( "generate account err")
	}
	fmt.Println( "accountO Key:11, accountO")
	//4,chains
	chain0, err := account0.NewChildKey(0)
	if err != nil {
		panic( "generate chain err")
	}
	fmt.Println("chainO Key:11, chain0")

	//5.Address
	addressO, err := chain0.NewChildKey(0)
	if err != nil {
		panic("generate address err")
	}
	fmt.Println( "addressO Key:11, byteToHex(address0-Key)")
	fmt.Println( "length:11, len(address0-Key)")
	//6.generate Address
	//eth address
	// public Key
	address0PubKey := addressO.PublicKey()
	fmt.Println("addressO public key:11, address0PubKey")
	//public key hash
	publicKeyHash := crypto.Keccak256(address0PubKey.Key[1:])
	fmt.Println(len(publicKeyHash))
	address := byteToHex(publicKeyHash[12:])
	fmt.Println("Ox11" + address)
	fmt.Println(len(address))

}

func byteToHex(bytes []byte) string {
	result := fmt.Sprintf("%x", bytes)
	return result
}