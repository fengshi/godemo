package main

import (
	"fmt"
	"goDemo/test/bitcoin_address/wal"
)

func main() {
	wallet := wal.NewWallet()

	wal.NewWallet()
	fmt.Println("0 - Having a private ECDSA key")

	fmt.Println(wal.ByteString(wallet.PrivateKey))
	fmt.Println("=======================")
	// fmt.Println("private wallet import format")
	// fmt.Println("private wallet import format", ToWIF(wallet.PrivateKey))
	// fmt.Println("=======================")
	fmt.Println("1 - Take the corresponding public key generated with it (65 bytes, 1 byte 0x04, 32 bytes corresponding to X coordinate, 32 bytes corresponding to Y coordinate)")
	fmt.Println("raw public key", wal.ByteString(wallet.PublicKey))
	fmt.Println("=======================")
	wallet.GetAddress()
}
