package main

import (
	log "github.com/cihub/seelog"
	"fmt"
	"goDemo/test/seelog/testLog"
)

func main() {
	defer log.Flush()

	//加载配置文件
	logger, err := log.LoggerFromConfigAsFile("api2.xml")

	if err!=nil{

		fmt.Println("parse config.xml error")
	}

	//替换记录器
	log.ReplaceLogger(logger)

	//for i:=0;i<10;i++{
	//
	//	log.Info("Hello from Seelog!")
	//}
	testLog.PLog()

	// 输出内容
	log.Trace("Test messagedddd!")
	log.Debug("Debug Printed")
	log.Info("Info Printed")
	log.Warn("Warn Printed")

	log.Error("Error Printed")
	log.Critical("Critical Printed")

}

