package goQueryTest

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strings"
)

func ByElement()  {
	html := `<body>
				<div>DIV1</div>
				<div>DIV2</div>
				<span>SPAN</span>
			</body>
			`
	dom,err:=goquery.NewDocumentFromReader(strings.NewReader(html))
	if err!=nil{
		fmt.Println(err)
	}

	dom.Find("div").Each(func(i int, selection *goquery.Selection) {
		fmt.Println(selection.Text())
	})
}

func ByID()  {
	html := `<body>

				<div id="div1">DIV1</div>
				<div class="div2">DIV2</div>
				<span>SPAN</span>
			</body>
			`
	dom,err:=goquery.NewDocumentFromReader(strings.NewReader(html))
	if err!=nil{
		fmt.Println(err)
	}
	dom.Find("#div1").Each(func(i int, selection *goquery.Selection) {
		fmt.Println("id 选择",selection.Text())
	})

	dom.Find(".div2").Each(func(i int, selection *goquery.Selection) {
		fmt.Println("class 选择",selection.Text())
	})
}

func ByIDClass()  {
	html := `<body>
				<div id="div1">DIV1</div>
				<div class="name">DIV2</div>
				<span>SPAN</span>
			</body>
			`
	dom,err:=goquery.NewDocumentFromReader(strings.NewReader(html))
	if err!=nil{
		fmt.Println(err)
	}
	dom.Find("div#div1").Each(func(i int, selection *goquery.Selection) {
		fmt.Println("div.id",selection.Text())
	})
	dom.Find("div[class=name]").Each(func(i int, selection *goquery.Selection) {
		fmt.Println("div.class",selection.Text())
	})


}