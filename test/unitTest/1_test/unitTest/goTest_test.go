package main

import (
	"testing"
	"github.com/magiconair/properties/assert"
)



//普通测试
func TestAddFuc(t *testing.T)  {


	assert.Equal(t, AddFunc(1, 3), "4")
	if AddFunc(1, 3) == 4 {
		t.Log("addfunc-测试通过")
	} else {
		t.Errorf("a=%v  b=%v got %v expected %v", 1, 3, AddFunc(1, 3), 4)
	}
}



// 表格驱动测试
func TestCalcTriangle(t *testing.T)  {
	tests :=[]struct{a, b, c int} {
		{3, 4, 5},
		{5, 12, 13},
		{8, 15, 17},
		{3000, 4000, 5001},

	}


	for _, te := range  tests {

		if re := CalcTriangle(te.a, te.b); re != te.c {

			t.Errorf("calcTriangle(%d, %d); "+
				"got %d; expected %d",
				te.a, te.b, te.c, re)

		} else {
			t.Log("测试通过")
		}
	}


}

