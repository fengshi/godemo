package main

import (
	"math"
)


func AddFunc(a, b int) int  {
	return a + b
}

func CalcTriangle(a, b int) int  {
	return int(math.Sqrt(float64(a*a + b*b)))
}

