package main

import (
	"fmt"
	"time"

	workerPool "goDemo/HighConcurrency/server/workerPool"
)

func main() {

	// generate worker to do job
	dispatcher := workerPool.NewDispatcher(100)
	dispatcher.Run()

	start := time.Now().UnixNano()
	// produce job to be done
	producer := workerPool.NewProducer(1000000)
	producer.Run()

	fmt.Println(time.Now().UnixNano() - start)
	time.Sleep(time.Second * 10000000)

}
