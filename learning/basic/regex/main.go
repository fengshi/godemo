package main

import (
	"fmt"
	"regexp"
)

/*
. 匹配任意字符
+ 一个或者多个
* 0个或者多个

.+@ 表示@前面有1个或者多个字符
*/

const (
	text1 = "My email is ggmouse@gmail.com"
	text2 = "ggmouse@gmail.com"
	text3 = "@gmail.com"
	text4 = "My email is ggmouse@gmail.com@asd.cn"
	text5 = `My email is ggmouse@gmail.com
	email1 is abc@def.com
	email2 is kk@qq.cn
	email3 is bb@qq.com.cn
`
)
func main() {
	re, err := regexp.Compile("ggmouse@gmail.com")
	if err != nil {
		return
	}

	res := re.FindString(text1)
	fmt.Println("0",res)

	// 从字符串中查找邮箱
	//xx@xx.xx    xx只能是字母或者数字
	re, err = regexp.Compile(`[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9.]+`)

	res = re.FindString(text1)
	fmt.Println("1",res)

	res = re.FindString(text2)
	fmt.Println("2",res)

	res = re.FindString(text3)
	fmt.Println("3",res)

	res = re.FindString(text4)
	fmt.Println("4",res)

	// -1 表示查找所有匹配到的值
	resArr := re.FindAllString(text5, -1)
	fmt.Println("5",resArr)


	// 从邮箱中提取需要的字段
	// 使用（）提取
	fmt.Println("提取")
	re, err = regexp.Compile(`([a-zA-Z0-9]+)@([a-zA-Z0-9]+)\.([a-zA-Z0-9.]+)`)
	arr := re.FindAllStringSubmatch(text5, -1)
	for _, m := range arr {
		fmt.Println(m)
	}

}


