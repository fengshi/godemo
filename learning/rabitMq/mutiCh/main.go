package mutiCh

import (
	"bytes"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"os"
	"strings"
	"time"
)

//多个生产者 消费者
func main() {
	conn, err := amqp.Dial("amqp://admin:rabbitmq123@18.232.146.30:5672/")
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	defer ch.Close()
	q, err := ch.QueueDeclare("hello", false, false, false, false, nil)

	body := bodyFrom(os.Args)

	//生产者
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/plain",
		Body:         []byte(body),
	})

	//消费者
	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	fmt.Println(err, "Failed to register a consumer")

	go func() {
		for msg := range msgs {
			log.Printf("Received a message: %s", msg.Body)
			dotCount := bytes.Count(msg.Body, []byte("."))
			t := time.Duration(dotCount)
			time.Sleep(t * time.Second)
			log.Printf("Done")
		}
	}()

	/*
	消息ack
	当消费者的autoack为true时，一旦收到消息就会直接把该消息设置为删除状态，
	如果消息的处理时间之内，消费者挂掉了那么这条消息就会丢失掉。
	rabbitmq支持消息ack机制，将autoack设为false，
	当处理完毕再手动触发ack操作。如果处理消息的过程中挂掉了，那么这条消息就会分发给其他都消费者。
	*/

	msgs, err = ch.Consume(q.Name, "", false, false, false, false, nil)
	fmt.Println(err, "Failed to register a consumer")


	go func() {
		for msg := range msgs {
			log.Printf("Received a message: %s", msg.Body)
			dotCount := bytes.Count(msg.Body, []byte("."))
			t := time.Duration(dotCount)
			time.Sleep(t * time.Second)
			log.Printf("Done")
			msg.Ack(false)
		}
	}()


}
func bodyFrom(args []string) string {
	if len(args) > 1 {
		return strings.Join(args[1:], " ")
	}
	return "default"
}
