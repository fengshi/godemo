package ch

import (
	"github.com/streadway/amqp"
	"log"
)

//https://blog.csdn.net/lastsweetop

//单一的生产者 消费者
func main()  {
	conn, err := amqp.Dial("amqp://admin:rabbitmq123@18.232.146.30:5672/")
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	defer ch.Close()

	q, err := ch.QueueDeclare("hello", false, false, false, false, nil)

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)


	go func() {
		for msg := range msgs {
			log.Printf("Received a message: %s", msg.Body)
		}
	}()

	body := "Hello World33!"

	err = ch.Publish("", q.Name, false, false, amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte(body),
	})

}