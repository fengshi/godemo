package main

import (
	"log"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
func main() {
	url := "amqp://user:password@192.168.101.151:5672"
	go pub(url)
	go pub(url)
	go pub(url)
	go pub(url)

	sub(url)
}

func pub(url string) {
	conn, err := amqp.Dial(url)
	failOnError(err, "拨号错误")
	defer conn.Close()
	ch, err := conn.Channel()
	failOnError(err, "创建信道错误")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"hello_exc",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, "声明交换器错误")

	// q, err := ch.QueueDeclare(
	// 	"hello_queue",
	// 	false,
	// 	false,
	// 	false,
	// 	false,
	// 	nil,
	// )
	// failOnError(err, "Failed to declare a queue")

	body := "hello world!!!"
	err = ch.Publish(
		"hello_exc",
		//q.Name,
		"xx_xx",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		},
	)
	log.Printf("[x] Sent %s", body)
	failOnError(err, "fail to publish a message")
}

func sub(url string) {
	conn, err := amqp.Dial(url)
	failOnError(err, "拨号错误")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello_queque",
		false,
		false,
		false,
		false,
		nil,
	)

	failOnError(err, "Failed to declare a queue")

	err = ch.QueueBind("hello_queque", "xx_xx", "hello_exc", false, nil)
	failOnError(err, "队列绑定错误")

	msgs, err := ch.Consume(
		q.Name,
		"hello_con",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a massage:%s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
