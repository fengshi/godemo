package main

import (
	"fmt"
	"log"
	"time"

	"github.com/streadway/amqp"
)

func init() {
	log.SetFlags(log.Ldate | log.Lshortfile | log.Ltime)
}

func main() {
	//url := "amqp://sui:SuiLiu123456@192.168.101.151:5674"
	//url := "amqp://admin:TY12ew3456ta1@192.168.101.74:5672"
	go pub("amqp://sui:SuiLiu123456@192.168.101.151:5674")
	//time.Sleep(2 * time.Second)
	//sub("amqp://sui:SuiLiu123456@192.168.101.151:5673")
	select {}
}

func pub(url string) {
	conn, err := amqp.Dial(url)
	//failOnError(err, "拨号错误")
	if err != nil {
		log.Fatalf("dail error:%v", err)
	}
	defer conn.Close()
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("创建信道错误:%v", err)
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"hello_exc",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		log.Fatalf("声明交换器错误:%v", err)
	}
	// n, err := ch.QueueDelete("hello_queue", false, false, false)
	// if err != nil {
	// 	log.Fatalf("删除队列:%v,%v", n, err)
	// }

	// log.Printf("删除队列:%v,%v", n, err)
	q, err := ch.QueueDeclare(
		"hello_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("声明队列错误:%v", err)
	}
	i := 0
	body := "hello world!!!"
	var s string
	for {
		i++
		go func(j int) {
			for {
				fmt.Println("----------------", j)
				s = fmt.Sprintf("%s++++[%d]", body, j)
				err = ch.Publish(
					//"hello_exc",
					"",
					q.Name,
					//"xx_xx",
					false,
					false,
					amqp.Publishing{
						ContentType: "text/plain",
						Body:        []byte(s),
					},
				)
				log.Printf("[x] Sent %s", s)

				if err != nil {
					log.Fatalf("fail to publish a message:"+fmt.Sprintf("%v", conn != nil)+":%v", err)
				}

				time.Sleep(time.Second)
			}
		}(i)
		time.Sleep(time.Second)
		if i == 2 {
			break
		}
	}
	select {}
}

func sub(url string) {
	time.Sleep(time.Second)
	conn, err := amqp.Dial(url)
	if err != nil {
		log.Fatalf("dail error:%v", err)
	}
	defer conn.Close()
	for i := 0; i < 2; i++ {
		go func(i int) {
			ch, err := conn.Channel()
			if err != nil {
				log.Fatalf("Failed to open a channel:%v", err)
			}
			defer ch.Close()

			// err = ch.ExchangeDeclare(
			// 	"hello_exc",
			// 	"direct",
			// 	false,
			// 	false,
			// 	false,
			// 	false,
			// 	nil,
			// )

			// failOnError(err, "声明交换器错误")
			// n, err := ch.QueueDelete("hello_queue", false, false, false)
			// if err != nil {
			// 	log.Fatalf("删除队列:%v,%d", err, n)
			// }

			q, err := ch.QueueDeclare(
				"hello_queue",
				false,
				false,
				false,
				false,
				nil,
			)

			if err != nil {
				log.Fatalf("Failed to declare a queue:%v", err)
			}

			err = ch.QueueBind(q.Name, q.Name, "hello_exc", false, nil)
			if err != nil {
				log.Fatalf("队列绑定错误:%v", err)
			}
			msgs, err := ch.Consume(
				q.Name,
				"hello_con",
				true,
				false,
				false,
				false,
				nil,
			)
			if err != nil {
				log.Fatalf("Failed to register a consumer:%v", err)
			}
			//go func() {
			for d := range msgs {
				log.Printf("[ch%d]:Received a massage:%s", i, d.Body)
			}
			//}()

		}(i)
	}

	forever := make(chan bool)

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
