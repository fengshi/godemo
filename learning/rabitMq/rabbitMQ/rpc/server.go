package main

import (
	"log"
	"strconv"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func fib(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}

func main() {

	url := "amqp://user:password@192.168.101.151:5672"
	conn, err := amqp.Dial(url)
	failOnError(err, "拨号错误")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "获取channel错误")

	err = ch.ExchangeDeclare(
		"rpc",
		"direct",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "声明Exchange")
	q, err := ch.QueueDeclare(
		"rpc_queue",
		false,
		false,
		false,
		false,
		nil)
	failOnError(err, "声明Queue")

	err = ch.Qos(
		1,
		0,
		false,
	)
	failOnError(err, "qos error")

	msg, err := ch.Consume(
		q.Name,
		"rpc_server",
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "consumer")

	forever := make(chan bool)

	go func() {

		for d := range msg {
			n, err := strconv.Atoi(string(d.Body))

			failOnError(err, "Failed to convert body to integer")

			log.Printf(" [.] fib(%d)", n)
			response := fib(n)
			err = ch.Publish(
				"",
				d.ReplyTo,
				false,
				false,
				amqp.Publishing{
					ContentType:   "text/plain",
					CorrelationId: d.CorrelationId,
					Body:          []byte(strconv.Itoa(response)),
				},
			)
			failOnError(err, "Failed to publish a message")

			d.Ack(false)
		}

	}()

	log.Printf(" [*] Awaiting RPC requests")
	<-forever
}
