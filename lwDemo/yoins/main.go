package main

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"myTool/common"
	"myTool/xpath"
	"os"
)

func main()  {

	url := "https://us.yoins.com/theme-Autumn-t-216213-fc-0-fv-0-s-6-price-0.00-0.00-p_2.html?noThird=1"
	doc := xpath.Fetch(url)

	nodes := htmlquery.Find(doc,`//div[@class="products-list-item"]/ul[@id="js-products-ul"]/li`)

	dir := os.ExpandEnv("$HOME") + "/Desktop/yoins2"

	fmt.Println("count", len(nodes))
	for _, n := range nodes {
		url := htmlquery.FindOne(n,`/div[1]/a[@class="link"]/img/@data-src`)

		u := htmlquery.InnerText(url)

		//u2 := htmlquery.SelectAttr(url,"src")
		//
		//u3 := htmlquery.OutputHTML(url, true)
		fmt.Println(u)

		price := htmlquery.FindOne(n, `/div[3]/span/text()`)

		p := htmlquery.InnerText(price)

		fmt.Println(p)

		fp := dir + "/" + p + ".jpg"
		common.DownLoadNoPass(u, fp)
	}

}
