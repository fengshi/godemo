package main

import (
	"fmt"
	"github.com/nfnt/resize"
	"image"
	"image/draw"
	"image/jpeg"
	"myTool/file"
	"myTool/img"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	path := "/Users/qianjianeng/Desktop/111"
	files, _ := file.GetAllFiles(path)

	worksDir := "/Users/qianjianeng/Desktop/112/"

	for _, file := range files {
		fmt.Println(file)


		img,_ := img.GetImageObj(file)
		des := image.NewRGBA(image.Rect(0, 0, img.Bounds().Max.X, img.Bounds().Max.X))
		draw.Draw(des, img.Bounds(), img, img.Bounds().Min, draw.Over)

		name := filepath.Base(file)
		name = strings.Split(name,".")[0]
		name = name + ".jpg"
		fSave, _ := os.Create(worksDir + name)


		defer fSave.Close()

		var opt jpeg.Options
		opt.Quality = 100

		newImage := resize.Resize(1024, 0, des, resize.Lanczos3)

		_ = jpeg.Encode(fSave, newImage, &opt) // put quality to 80%


	}






}


