package main

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"myTool/common"
	"myTool/csv"
	"myTool/file"
	"myTool/xpath"
	"net/http"
	"os"
	"strconv"
	"strings"
)


var (
	inputPath =  "./11.csv"
	outputDir = os.ExpandEnv("$HOME") +"/Desktop" +"/club"
)

func main()  {

	fmt.Println(os.ExpandEnv("$HOME"))
	fmt.Println(outputDir)
	if !file.PathExist(outputDir) {
		_=os.MkdirAll(outputDir, os.ModePerm)
	}



	buf := csv.ReadData(inputPath)

	var IDS []int
	for i:= 1;i< len(buf); i++ {
		ID := buf[i][0]
		if len(ID) < 5 {
			continue
		}
		ID = strings.TrimSuffix(ID, "N")
		ID = ID[4:]
		IDInt,err := strconv.Atoi(ID)
		if err != nil {
			fmt.Println("error ---index",i)
			continue
		}

		IDS = append(IDS, IDInt)
	}

	fmt.Println("数量：", len(IDS) - 1)

	dir := outputDir
	for _, ID := range IDS {

		url := fmt.Sprintf("https://www.clubfactory.com/product/%v", ID)

		doc := xpath.FetchHeadCookie(url,nil, GetCookies())

		if doc == nil {
			continue
		}

		//nodes := htmlquery.Find(doc, `//div[@class="rank-list-wrap"]/ul[@class="rank-list"]/li[@class="rank-item"]`)
		imgNode := htmlquery.FindOne(doc, `//div[@class="preview"]/img/@src`)
		priceNode := htmlquery.FindOne(doc, `//span[@class="price-num"]/text()`)
		imageUrl := htmlquery.InnerText(imgNode)
		imagePrice := htmlquery.InnerText(priceNode)
		imageUrl = "https:" + imageUrl
		fmt.Println(imageUrl)
		fmt.Println(imagePrice)

		fileName := fmt.Sprintf("%v_%v.jpg", ID, imagePrice)
		path := dir + "/" + fileName
		common.DownLoadNoPass(imageUrl, path)

	}

}


func GetCookies()[]*http.Cookie  {
	cookName := &http.Cookie{Name:"gender", Value:"U"}
	cook1 := &http.Cookie{Name:"guest_id", Value:"98d75e466a6af0e88edd4893397c9a34"}
	cook2 := &http.Cookie{Name:"_ga", Value:"GA1.2.1594389991.1560325744"}
	cook3 := &http.Cookie{Name:"_gid", Value:"GA1.2.1346961380.1567480866"}
	cook4 := &http.Cookie{Name:"language_code", Value:"es-MX"}
	cook5 := &http.Cookie{Name:"country_code", Value:"in"}
	cook6 := &http.Cookie{Name:"symbol", Value:"%u20B9"}

	var res []*http.Cookie
	res = append(res, cookName)
	res = append(res, cook1)
	res = append(res, cook2)
	res = append(res, cook3)
	res = append(res, cook4)
	res = append(res, cook5)
	res = append(res, cook6)
	return res
}
