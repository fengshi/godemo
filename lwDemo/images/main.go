package main

import (
	"fmt"
	"myTool/common"
	"myTool/csv"
	"myTool/file"
	"os"
	"path/filepath"
)

func main() {
	pricemap := make(map[string]int)
	path := os.ExpandEnv("$HOME") + "/Desktop"+"/CF参考商品.csv"
	fmt.Println(path)

	if file.PathExist(path) == false {
		path = os.ExpandEnv("$HOME") + "/Desktop"+"/123.csv"
		if file.PathExist(path) == false {
			panic("文件不存在")
		}
	}

	fmt.Println("将 CF参考商品.csv 放在桌面")
	fmt.Println("下载目录：下载/CFImages")
	data := csv.ReadData(path)
	DownLoadDir := os.ExpandEnv("$HOME") + "/Downloads/CFImages"

	files, err := file.GetAllFiles(DownLoadDir)
	if err == nil {
		for _, file := range files {
			_ = os.Remove(file)
		}
	}

	os.MkdirAll(DownLoadDir, 0777)
	var imagepath = ""
	for index, v := range data {
		if index == 0 {
			 continue
		}
		link := v[1]
		price := v[13]

		fmt.Println(link, price)
		if _,ok := pricemap[price];ok {

			count := pricemap[price]
			imagepath = fmt.Sprintf("/%v_%v.jpg", price,count +1)

		} else {
			imagepath = fmt.Sprintf("/%v.jpg",price)
		}

		common.Download(link, filepath.Join(DownLoadDir,imagepath))
		pricemap[price] = pricemap[price] + 1

		//fmt.Println(link, imagepath)

	}

}
